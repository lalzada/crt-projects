import { Routes } from '@angular/router';
import {OverviewComponent} from "./overview/overview.component";
import {SettingsComponent} from "./settings/settings.component";
import {VoucherManagementComponent} from "./voucher_management/voucher_management.component";

export const CaptivePortalRoutes: Routes = [
    {
        path: '',
        data: {
            breadcrumb: 'Captive Portal',
            status: false
        },
        children: [
            {
                path: 'overview',
                component: OverviewComponent,
                data: {
                    breadcrumb: 'Overview',
                    status: true
                }
            }, {
                path: 'settings',
                component: SettingsComponent,
                data: {
                    breadcrumb: 'Settings',
                    status: true
                }
            }, {
                path: 'voucher-management',
                component: VoucherManagementComponent,
                data: {
                    breadcrumb: 'Voucher Management',
                    status: true
                }
            }
        ]
    }
];
