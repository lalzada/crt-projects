import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CaptivePortalComponent } from './captive_portal.component';
import { OverviewComponent } from './overview/overview.component';
import {RouterModule} from "@angular/router";
import {CaptivePortalRoutes} from "./captive_portal.routing";
import {SettingsComponent} from "./settings/settings.component";
import { VoucherManagementComponent } from './voucher_management/voucher_management.component';
import {SharedModule} from "../../shared/shared.module";

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CaptivePortalRoutes),
    SharedModule
  ],
  declarations: [
    CaptivePortalComponent,
    OverviewComponent,
    SettingsComponent,
    VoucherManagementComponent
  ]
})
export class CaptivePortalModule { }
