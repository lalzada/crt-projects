import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-captive-portal',
  template: '<router-outlet><spinner></spinner></router-outlet>'
})
export class CaptivePortalComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
