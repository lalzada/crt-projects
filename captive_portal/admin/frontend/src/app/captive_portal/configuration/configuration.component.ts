import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-configuration',
  template: '<router-outlet><spinner></spinner></router-outlet>'
})
export class ConfigurationComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
