import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfigurationComponent } from './configuration.component';
import { RolesComponent } from './roles/roles.component';
import {RouterModule} from "@angular/router";
import {ConfigurationRoutes} from "./configuration.routing";
import {UsersComponent} from "./users/users.component";
import {SharedModule} from "../../shared/shared.module";

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ConfigurationRoutes),
    SharedModule
  ],
  declarations: [
    ConfigurationComponent,
    RolesComponent,
    UsersComponent
  ]
})
export class ConfigurationModule { }
