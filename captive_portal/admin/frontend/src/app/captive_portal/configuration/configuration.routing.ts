import { Routes } from '@angular/router';
import {RolesComponent} from "./roles/roles.component";
import {UsersComponent} from "./users/users.component";

export const ConfigurationRoutes: Routes = [
    {
        path: '',
        data: {
            breadcrumb: 'Configuration',
            status: false
        },
        children: [
            {
                path: 'roles',
                component:RolesComponent,
                data: {
                    breadcrumb: 'Roles',
                    status: true
                }
            }, {
                path: 'users',
                component: UsersComponent,
                data: {
                    breadcrumb: 'Users',
                    status: true
                }
            }
        ]
    }
];
