import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { settings } from './../settings';

@Injectable()
export class CaptivePortalService {
  constructor (
    private http: Http,
  ) {
  }

  overview = () => {
    return this.http
    .get(`${settings.api_url}/captive-portal/overview`)
    .map((res:Response) => res.json());
  }

  captive_portal_settings = () => {
    return this.http
    .get(`${settings.api_url}/captive-portal/settings`)
    .map((res:Response) => res.json());
  }

  vouchers = () => {
    return this.http
    .get(`${settings.api_url}/captive-portal/voucher/list`)
    .map((res:Response) => res.json());
  }

  packages = () => {
    return this.http
    .get(`${settings.api_url}/captive-portal/package/list`)
    .map((res:Response) => res.json());
  }

}