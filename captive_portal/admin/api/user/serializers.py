from rest_framework.exceptions import AuthenticationFailed
from rest_framework import serializers
from django.contrib.auth import authenticate
from .models import Token
from configuration.models import User, UserRole, RolePermission


class LoginSerializer(serializers.Serializer):

    username = serializers.CharField()
    password = serializers.CharField(write_only=True)
    first_name = serializers.CharField(read_only=True)
    last_name = serializers.CharField(read_only=True)
    email = serializers.EmailField(read_only=True)
    phone = serializers.CharField(read_only=True)
    token = serializers.CharField(read_only=True)
    is_superuser = serializers.BooleanField(read_only=True)
    roles = serializers.SerializerMethodField(read_only=True)
    address = serializers.CharField(read_only=True)
    state = serializers.CharField(read_only=True)
    city = serializers.CharField(read_only=True)
    country = serializers.CharField(read_only=True)

    class Meta:
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        try:
            self.user = args[0]
        except:
            self.user = None

        super().__init__(*args, **kwargs)

    def validate(self, data):
        super().validate(data)
        username = data.get('username')
        password = data.get('password')

        if username and password:
            user = authenticate(self.context['request'], username=username, password=password)
            if not user:
                raise AuthenticationFailed(detail='invalid username or password')

            if not user.token:
                user.token = Token()
                user.save()

            self.user = User.objects.get(username=username)

        return data

    @staticmethod
    def get_permissions(role):
        return [perm.get_perm_dict() for perm in RolePermission.objects.filter(role=role.to_dbref())]

    def get_role_dict(self, role):
        return {
                "name": role.name,
                "permissions": self.get_permissions(role)
            }

    def get_roles(self, obj):
        return [self.get_role_dict(userRole.role) for userRole in UserRole.objects().filter(user=self.user.to_dbref())]
