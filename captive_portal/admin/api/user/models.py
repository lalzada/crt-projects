from mongoengine import EmbeddedDocument
from mongoengine import fields
import binascii
import os


class Token(EmbeddedDocument):
    key = fields.StringField(max_length=44)
    created = fields.DateTimeField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not self.key:
            self.key = self._generate_key()

    @staticmethod
    def _generate_key():
        return binascii.hexlify(os.urandom(22)).decode()

    def __unicode__(self):
        return self.key



