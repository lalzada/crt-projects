from rest_framework_mongoengine import generics
from rest_framework.response import Response
from .serializers import LoginSerializer
from configuration.models import User


class LoginView(generics.GenericAPIView):
    serializer_class = LoginSerializer
    authentication_classes = ()
    permission_classes = ()

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = User.objects.get(username=serializer.data.get('username'))
        serializer = self.get_serializer(user)
        return Response(serializer.data)




