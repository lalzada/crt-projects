from rest_framework_mongoengine.serializers import DocumentSerializer
from rest_framework import serializers
from bson import ObjectId
from captive_portal import models
from configuration.models import Provider


class SettingsSerializer(DocumentSerializer):

    login_timeout = serializers.IntegerField(min_value=0, max_value=999999999999999999)

    class Meta:
        fields = '__all__'
        model = models.Settings


class ProviderSerializer(DocumentSerializer):

    class Meta:
        fields = '__all__'
        model = Provider


class SocialLoginSerializer(serializers.Serializer):

    id = serializers.CharField()
    is_active = serializers.BooleanField()

    class Meta:
        fields = ('id', 'is_active')


class AllowedSocialLoginSerializer(serializers.Serializer):
    social_logins = serializers.ListField(child=SocialLoginSerializer())

    class Meta:
        fields = '__all__'

    @staticmethod
    def validate_social_logins(social_logins):

        if len(social_logins) < 1:
            raise serializers.ValidationError('expected at least one social provider object')

        for data in social_logins:
            try:
                ObjectId(data.get('id'))
            except:
                raise serializers.ValidationError('a valid object id is required for social provider')

            try:
                obj = Provider.objects.get(id=data.get('id'))
                obj.is_active = data.get('is_active')
                obj.save()
            except Provider.DoesNotExist:
                pass

        return social_logins

    def create(self, validated_data):
        return validated_data


class VoucherSerializer(DocumentSerializer):

    class Meta:
        model = models.Voucher
        fields = '__all__'


class PackageSerializer(DocumentSerializer):

    class Meta:
        model = models.Package
        fields = '__all__'


