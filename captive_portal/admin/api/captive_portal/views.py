from rest_framework_mongoengine import generics
from rest_framework.response import Response
from functools import reduce
from base.utils import convert_size
from captive_portal import serializers
from captive_portal import models
from configuration.models import Provider
from bson import ObjectId


class CaptivePortalOverview(generics.GenericAPIView):
    http_method_names = ['get']

    @staticmethod
    def get(request):
        data = dict()
        data['users_count'] = models.WifiUser.objects.count()
        data['guests_count'] = models.WifiGuest.objects.count()
        active_guests = models.WifiGuest.objects.filter(is_valid=True)
        # aggregate i.e sum() doesn't work in MongoDB 2.0, so traverse each record and sum it
        data_usage = reduce(
            lambda s, guest: s + guest.downloadTotal + guest.uploadTotal,
            active_guests, 0
        )
        data['data_usage'] = convert_size(data_usage)
        return Response(data)


class UpdateSettings(generics.UpdateAPIView):

    serializer_class = serializers.SettingsSerializer
    queryset = models.Settings.objects.all()
    object = None
    http_method_names = ['post']

    def post(self, request, *args, **kwargs):
        return self.put(request, *args, **kwargs)

    @staticmethod
    def get_object():
        if models.Settings.objects.count() < 1:
            return models.Settings.objects.create()

        return models.Settings.objects.first()


class ProviderList(generics.ListAPIView):
    serializer_class = serializers.ProviderSerializer
    queryset = Provider.objects.all()


class AllowedSocialLogin(generics.GenericAPIView):
    serializer_class = serializers.AllowedSocialLoginSerializer
    queryset = Provider.objects.all()
    http_method_names = ['post']

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)


class VoucherList(generics.ListAPIView):
    serializer_class = serializers.VoucherSerializer
    queryset = models.Voucher.objects.all()


class VoucherCreate(generics.CreateAPIView):
    model = models.Voucher
    serializer_class = serializers.VoucherSerializer


class VoucherEdit(generics.UpdateAPIView):
    queryset = models.Voucher.objects.all()
    serializer_class = serializers.VoucherSerializer
    http_method_names = ['post']

    def post(self, request, *args, **kwargs):
        try:
            ObjectId(kwargs.get('id'))
        except:
            return Response({'detail': 'invalid voucher id'})
        return self.patch(request, *args, **kwargs)


class VoucherDelete(generics.RetrieveDestroyAPIView):
    queryset = models.Voucher.objects.all()
    serializer_class = serializers.VoucherSerializer
    http_method_names = ['post']

    def get_object(self):
        return models.Voucher.objects.get(id=ObjectId(self.kwargs.get('id')))

    def post(self, request, *args, **kwargs):
        try:
            ObjectId(kwargs.get('id'))
        except:
            return Response({'detail': 'invalid voucher id'})
        return self.delete(request, *args, **kwargs)


class PackageList(generics.ListAPIView):
    queryset = models.Package.objects.all()
    serializer_class = serializers.PackageSerializer


class PackageCreate(generics.CreateAPIView):
    model = models.Package
    serializer_class = serializers.PackageSerializer


class PackageEdit(generics.UpdateAPIView):
    queryset = models.Package.objects.all()
    serializer_class = serializers.PackageSerializer
    http_method_names = ['post']

    def post(self, request, *args, **kwargs):
        try:
            ObjectId(kwargs.get('id'))
        except:
            return Response({'detail': 'invalid package id'})
        return self.patch(request, *args, **kwargs)


class PackageDelete(generics.RetrieveDestroyAPIView):
    queryset = models.Package.objects.all()
    serializer_class = serializers.PackageSerializer
    http_method_names = ['post']

    def get_object(self):
        return models.Package.objects.get(id=ObjectId(self.kwargs.get('id')))

    def post(self, request, *args, **kwargs):
        try:
            ObjectId(kwargs.get('id'))
        except:
            return Response({'detail': 'invalid package id'})
        return self.delete(request, *args, **kwargs)


