from django.urls import path
from captive_portal import views


app_name = 'captive_portal'

urlpatterns = [
    path('overview', views.CaptivePortalOverview.as_view(), name='overview'),
    path('update-settings', views.UpdateSettings.as_view(), name='update-settings'),
    path('social-provider/list', views.ProviderList.as_view(), name='social-provider-list'),
    path('allowed-social-login', views.AllowedSocialLogin.as_view(), name='allowed-social-login'),

    path('voucher/list', views.VoucherList.as_view(), name='voucher-list'),
    path('voucher/create', views.VoucherCreate.as_view(), name='voucher-create'),
    path('voucher/edit/<str:id>', views.VoucherEdit.as_view(), name='voucher-edit'),
    path('voucher/delete/<str:id>', views.VoucherDelete.as_view(), name='voucher-delete'),

    path('package/list', views.PackageList.as_view(), name='package-list'),
    path('package/create', views.PackageCreate.as_view(), name='package-create'),
    path('package/edit/<str:id>', views.PackageEdit.as_view(), name='package-edit'),
    path('package/delete/<str:id>', views.PackageDelete.as_view(), name='package-delete'),

]
