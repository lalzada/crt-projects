from rest_framework_mongoengine.serializers import DocumentSerializer, drf_fields
from rest_framework import serializers
from bson import ObjectId
from configuration import models


class RoleSerializer(DocumentSerializer):

    class Meta:
        model = models.Role
        fields = '__all__'


class RoleCreateSerializer(serializers.Serializer):

    name = serializers.CharField()
    permissions = serializers.ListField(child=serializers.CharField())

    class Meta:
        fields = '__all__'

    def create(self, validated_data):
        role = models.Role.objects.create(name=validated_data.get('name'))
        for perm_id in validated_data.get('permissions'):
            try:
                ObjectId(perm_id)
            except:
                break

            try:
                # check if permission exist in db
                perm = models.Permission.objects.get(id=perm_id)
                try:
                    # check if permission already exist for this role
                    models.RolePermission.objects.get(role=role.to_dbref(), permission=perm.to_dbref())
                except models.RolePermission.DoesNotExist:
                    models.RolePermission.objects.create(role=role.to_dbref(), permission=perm.to_dbref())

            except models.Permission.DoesNotExist:
                pass

        return validated_data

    @staticmethod
    def validate_name(value):
        try:
            models.Role.objects.get(name__iexact=value)
            raise serializers.ValidationError('A role with same name already exist')
        except models.Role.DoesNotExist:
            return value

    @staticmethod
    def validate_permissions(value):
        if len(value) < 1:
            raise serializers.ValidationError('Expected at least a permission id')

        for perm_id in value:
            try:
                ObjectId(perm_id)
            except:
                raise serializers.ValidationError('permission id must be a valid mongo object id')
        return value


class RoleEditSerializer(serializers.Serializer):

    name = serializers.CharField()
    permissions = serializers.ListField(child=serializers.CharField())

    class Meta:
        fields = '__all__'

    def update(self, instance, validated_data):
        role = instance
        role.name = validated_data.get('name', role.name)
        roles_permissions = models.RolePermission.objects.filter(role=role.to_dbref())
        roles_permissions.delete()
        for perm_id in validated_data.get('permissions'):
            try:
                ObjectId(perm_id)
            except:
                continue

            try:
                # check if permission exist in db
                perm = models.Permission.objects.get(id=perm_id)
                try:
                    # check if permission already exist for this role
                    models.RolePermission.objects.get(role=role.to_dbref(), permission=perm.to_dbref())
                except models.RolePermission.DoesNotExist:
                    models.RolePermission.objects.create(role=role.to_dbref(), permission=perm.to_dbref())

            except models.Permission.DoesNotExist:
                pass

        return validated_data

    @staticmethod
    def validate_permissions(value):
        if len(value) < 1:
            raise serializers.ValidationError('Expected at least a permission id')

        for perm_id in value:
            try:
                ObjectId(perm_id)
            except:
                raise serializers.ValidationError('permission id must be a valid mongo object id')
        return value


class RoleListSerializer(DocumentSerializer):

    permissions = drf_fields.SerializerMethodField()

    class Meta:
        model = models.Role
        fields = '__all__'

    @staticmethod
    def get_permissions(obj):
        return [perm.get_perm_dict() for perm in models.RolePermission.objects.filter(role=obj.to_dbref())]


class UserSerializer(DocumentSerializer):

    password = drf_fields.CharField(min_length=6, max_length=128, write_only=True)

    class Meta:
        model = models.User
        fields = '__all__'
        read_only_fields = ('is_superuser', 'token')

    @staticmethod
    def validate_username(value):
        try:
            models.User.objects.get(username=value)
            raise serializers.ValidationError('username already taken. try a different username')
        except models.User.DoesNotExist:
            return value


class UserListSerializer(DocumentSerializer):

    roles = drf_fields.SerializerMethodField()

    class Meta:
        model = models.User
        exclude = ('password', 'token')

    @staticmethod
    def get_roles(obj):
        print([role.role.name for role in models.UserRole.objects().filter(user=obj.to_dbref())])
        return [role.role.name for role in models.UserRole.objects().filter(user=obj.to_dbref())]


class UserEditSerializer(serializers.Serializer):

    first_name = serializers.CharField(required=False)
    last_name = serializers.CharField(required=False)
    roles = serializers.ListField(child=serializers.CharField())

    class Meta:
        fields = '__all__'

    def update(self, instance, validated_data):
        user = instance
        user.first_name = validated_data.get('first_name', instance.first_name)
        user.last_name = validated_data.get('last_name', instance.last_name)
        user.save()
        user_roles = models.UserRole.objects.filter(user=user.to_dbref())
        user_roles.delete()
        for role_id in validated_data.get('roles'):
            try:
                ObjectId(role_id)
            except:
                continue

            try:
                # check if role exist in db
                role = models.Role.objects.get(id=role_id)
                try:
                    # check if role already exist for this user
                    models.UserRole.objects.get(role=role.to_dbref(), user=user.to_dbref())
                except models.UserRole.DoesNotExist:
                    models.UserRole.objects.create(role=role.to_dbref(), user=user.to_dbref())

            except models.Role.DoesNotExist:
                pass

        return validated_data

    @staticmethod
    def validate_roles(value):
        if len(value) < 1:
            raise serializers.ValidationError('Expected at least a role id')

        for role_id in value:
            try:
                ObjectId(role_id)
            except:
                raise serializers.ValidationError('role id must be a valid mongo object id')
        return value
