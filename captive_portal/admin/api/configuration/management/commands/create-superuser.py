from django.core.management.base import BaseCommand
from configuration.models import User
from user.models import Token


class Command(BaseCommand):
    help = 'create superuser'

    def add_arguments(self, parser):
        while True:
            username = input('Enter username: ')
            if not username:
                self.stdout.write(self.style.ERROR('Error: username cannot be blank'))
                continue

            try:
                User.objects.get(username=username)
                self.stdout.write(self.style.ERROR('Error: username is already been taken. Try a different username'))
                continue
            except User.DoesNotExist:
                break

        while True:
            password = input('Enter password: ')
            if not password:
                self.stdout.write(self.style.ERROR('Error: password cannot be blank'))
                continue

            try:
                User.objects.create(
                    username=username,
                    password=password,
                    is_superuser=True
                )

                self.stdout.write(self.style.SUCCESS('superuser created successfully.'))
                break
            except Exception as e:
                self.stdout.write(self.style.ERROR(e))
                break

    def handle(self, *args, **options):
        pass




