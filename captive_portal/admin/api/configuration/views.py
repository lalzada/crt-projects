from rest_framework_mongoengine import generics
from bson import ObjectId
from rest_framework.response import Response
from configuration import serializers
from configuration import models


class RoleCreate(generics.CreateAPIView):
    serializer_class = serializers.RoleCreateSerializer
    queryset = models.Role.objects.all()


class RoleEdit(generics.UpdateAPIView):
    serializer_class = serializers.RoleEditSerializer
    queryset = models.Role.objects.all()
    http_method_names = ['post']

    def post(self, request, *args, **kwargs):
        try:
            ObjectId(kwargs.get('id'))
        except:
            return Response({'detail': 'invalid role id'})
        return self.patch(request, *args, **kwargs)


class RoleList(generics.ListAPIView):
    serializer_class = serializers.RoleListSerializer
    queryset = models.Role.objects.all()


class RoleDelete(generics.RetrieveDestroyAPIView):
    queryset = models.Role.objects.all()
    serializer_class = serializers.RoleSerializer
    http_method_names = ['post']

    def get_object(self):
        return models.Role.objects.get(id=ObjectId(self.kwargs.get('id')))

    def post(self, request, *args, **kwargs):
        try:
            ObjectId(kwargs.get('id'))
        except:
            return Response({'detail': 'invalid role id'})
        return self.delete(request, *args, **kwargs)


class UserCreate(generics.CreateAPIView):
    serializer_class = serializers.UserSerializer
    queryset = models.User.objects.all()


class UserList(generics.ListAPIView):
    queryset = models.User.objects.filter(is_superuser=False)
    serializer_class = serializers.UserListSerializer


class UserDelete(generics.RetrieveDestroyAPIView):
    queryset = models.User.objects.all()
    serializer_class = serializers.UserSerializer
    http_method_names = ['post']

    def get_object(self):
        return models.User.objects.get(id=ObjectId(self.kwargs.get('id')))

    def post(self, request, *args, **kwargs):
        try:
            ObjectId(kwargs.get('id'))
        except:
            return Response({'detail': 'invalid user id'})
        return self.delete(request, *args, **kwargs)


class UserEdit(generics.UpdateAPIView):
    serializer_class = serializers.UserEditSerializer
    queryset = models.User.objects.all()
    http_method_names = ['post']

    def post(self, request, *args, **kwargs):
        try:
            ObjectId(kwargs.get('id'))
        except:
            return Response({'detail': 'invalid user id'})
        return self.patch(request, *args, **kwargs)
