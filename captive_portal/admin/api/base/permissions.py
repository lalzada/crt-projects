from rest_framework.permissions import BasePermission


class ViewPermissionRequired(BasePermission):
    """
    This class will check if the current visited View is permitted to logged in user
    """

    authenticated_users_only = True

    def has_permission(self, request, view):
        # Workaround to ensure DjangoModelPermissions are not applied
        # to the root view when using DefaultRouter.

        if not request.user or \
                (not request.user.is_authenticated and self.authenticated_users_only):
            return False

        view_name = view.__class__.__name__.lower()
        return request.user.has_perm(view_name)
