from rest_framework import authentication
from rest_framework.exceptions import AuthenticationFailed

from user.models import Token
from configuration.models import User


class TokenAuthentication(authentication.TokenAuthentication):
    """
    Simple token based authentication.

    Clients should authenticate by passing the token key in the "Authorization"
    HTTP header, prepended with the string "Token ".  For example:

    Authorization: Token 401f7ac837da42b97f613d789819ff93537bee6a
    """

    keyword = 'Token'
    model = Token

    def authenticate_credentials(self, key):
        """
        Overriding just to replace sql Token model with Mongo Token model
        """
        try:
            user = User.objects.get(token__key=key)
        except User.DoesNotExist:
            raise AuthenticationFailed('Invalid token.')

        return user, user.token

