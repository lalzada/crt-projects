"""
WSGI config for base project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.0/howto/deployment/wsgi/
"""

import os
import sys

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "../../")))
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "../")))

os.environ["DJANGO_SETTINGS_MODULE"] = "base.settings.staging"

from django.core.wsgi import get_wsgi_application

application = get_wsgi_application()

from django.conf import settings
from django.urls.resolvers import RegexPattern, URLResolver, URLPattern
from configuration.models import Permission

view_names = []


# Check if for all view names and add it to Permission model if not already exist

def get_view_names(urlpatterns):
    for pattern in urlpatterns:
        if isinstance(pattern, URLResolver):
            get_view_names(pattern.url_patterns)

        elif isinstance(pattern, RegexPattern):
            try:
                # python2 use func_name
                view_name = pattern.callback.func_name
            except AttributeError:
                # python3 use __name__
                view_name = pattern.callback.__name__

            view_names.append(view_name)

        elif isinstance(pattern, URLPattern):
            try:
                # python2 use func_name
                view_name = pattern.callback.func_name
            except AttributeError:
                # python3 use __name__
                view_name = pattern.callback.__name__

            view_names.append(view_name)

    return view_names


root_urlconf = __import__(settings.ROOT_URLCONF)
urls = root_urlconf.urls.urlpatterns
get_view_names(urls)

for view_name in view_names:
    try:
        Permission.objects.get(name=view_name)
    except Permission.DoesNotExist:
        Permission.objects.create(name=view_name)





