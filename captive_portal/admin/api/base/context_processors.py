# PermWrapper and PermLookupDict proxy the permissions system into objects that
# the template system can understand.


class PermLookupDict:
    def __init__(self, user, perm_name):
        self.user, self.perm_name = user, perm_name.lower()

    def __repr__(self):
        return str(self.user.get_all_permissions())

    def __iter__(self):
        # To fix 'item in perms' and __getitem__ interaction we need to
        # define __iter__. See #18979 for details.
        raise TypeError("PermLookupDict is not iterable.")

    def __bool__(self):
        return self.user.has_perm(self.perm_name)


class PermWrapper:
    def __init__(self, user):
        self.user = user

    def __getitem__(self, perm_name):
        return PermLookupDict(self.user, perm_name.lower())

    def __iter__(self):
        # I am large, I contain multitudes.
        raise TypeError("PermWrapper is not iterable.")

    def __contains__(self, perm_name):
        """
        Lookup by "viewName" in perms.
        """
        return bool(self[perm_name.lower()])


def auth(request):
    """
    Return context variables required by apps that use our Custom Authentication system

    If there is no 'user' attribute in the request, use AnonymousUser (from
    django.contrib.auth).
    """
    if hasattr(request, 'user'):
        user = request.user
    else:
        from django.contrib.auth.models import AnonymousUser
        user = AnonymousUser()

    return {
        'user': user,
        'perms': PermWrapper(user),
    }
