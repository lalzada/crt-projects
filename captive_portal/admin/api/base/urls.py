import os
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from rest_framework_swagger.views import get_swagger_view

urlpatterns = [
    path('dashboard/', include('dashboard.urls', namespace='dashboard')),
    path('captive-portal/', include('captive_portal.urls', namespace='captive_portal')),
    path('configuration/', include('configuration.urls', namespace='configuration')),
    path('clients-live-traffic/', include('clients_live_traffic.urls', namespace='clients_live_traffic')),
    path('user/', include('user.urls', namespace='user')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

baseUrl = '/'

if os.environ['DJANGO_SETTINGS_MODULE'] == 'base.settings.staging':
    baseUrl = '/captive-portal-admin-api/'

schema_view = get_swagger_view(
    title='Captive Portal Admin API', patterns=urlpatterns, url=baseUrl)

urlpatterns += [path('', schema_view)]
