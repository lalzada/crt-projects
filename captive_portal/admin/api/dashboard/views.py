from rest_framework.response import Response
from rest_framework_mongoengine import generics
from configuration.models import Device


class Dashboard(generics.GenericAPIView):

    @staticmethod
    def get(request):
        data = dict()
        devices = Device.objects.aggregate({"$group": {"_id": "$type", "value": {"$sum": 1},
                                                       "name": {"$first": "$type"}}})

        device_list = [
            {"name": device.get('name').capitalize() if device.get('name') else 'other', "value": device['value']}
            for device in devices
        ]
        data['device_list'] = device_list

        device_models = Device.objects.aggregate({"$group": {"_id": "$model", "value": {"$sum": 1},
                                                             "name": {"$first": "$model"}}})

        device_models = [
            {"name": model.get('name').capitalize() if model.get('name') else 'other', "value": model['value']}
            for model in device_models
        ]
        data['device_models'] = device_models

        os_list = Device.objects.aggregate({"$group": {"_id": "$operating_system", "value": {"$sum": 1},
                                                       "name": {"$first": "$operating_system"}}})

        os_list = [
            {"name": os.get('name').capitalize() if os.get('name') else 'other', "value": os['value']}
            for os in os_list
        ]
        data['os_list'] = os_list

        browser_list = Device.objects.aggregate({"$group": {"_id": "$browser", "value": {"$sum": 1},
                                                            "name": {"$first": "$browser"}}})

        browser_list = [
            {"name": browser.get('name').capitalize() if browser.get('name') else 'other', "value": browser['value']}
            for browser in browser_list
        ]
        data['browser_list'] = browser_list

        return Response(data)



