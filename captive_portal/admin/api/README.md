#### Captive Portal Admin Panel

###### Tools
1. Python 3.5
2. Django 2
3. MongoDB 3.2
4. [MongoEngine (Django ORM for MongoDB)](https://github.com/MongoEngine/mongoengine)
5. [Gentelella Bootstrap Template](https://colorlib.com/polygon/gentelella/index.html)


###### Setup

Install dependencies
```
pip3 install -r requirements/local.txt # /staging.txt, /production.txt
```

Run project
```
./manage.py runserver --settings=base.settings.local # .staging, .production
```

Settings are split into multiple files based on local, staging and production
