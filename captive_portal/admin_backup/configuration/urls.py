from django.urls import path
from configuration import views

app_name = 'configuration'

urlpatterns = [
    path('user/create', views.UserCreate.as_view(), name='user-create'),
    path('user/list', views.UserList.as_view(), name='user-list'),
    path('user/edit/<str:pk>', views.UserEdit.as_view(), name='user-edit'),
    path('user/delete/<str:pk>', views.UserDelete.as_view(), name='user-delete'),

    path('role/create', views.RoleCreate.as_view(), name='role-create'),
    path('role/edit/<str:pk>', views.RoleEdit.as_view(), name='role-edit'),
    path('role/delete/<str:pk>', views.RoleDelete.as_view(), name='role-delete'),
    path('role/list', views.RoleList.as_view(), name='role-list'),

]
