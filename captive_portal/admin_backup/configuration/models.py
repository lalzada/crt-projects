from mongoengine import fields, Document, CASCADE
from django.contrib.auth.hashers import check_password, make_password
from django.contrib import auth
from django.conf import settings
from django.contrib.auth.models import _user_has_perm


class Permission(Document):
    name = fields.StringField(required=True)

    def __unicode__(self):
        return self.name

    meta = {
        'indexes': [
            {'fields': ['name'], 'unique': True}
        ]
    }


class Role(Document):
    name = fields.StringField(unique=True, required=True)

    def __unicode__(self):
        return self.name

    meta = {
        'indexes': [
            {'fields': ['name']}
        ]
    }


class RolePermission(Document):
    role = fields.ReferenceField(Role, required=True, reverse_delete_rule=CASCADE)
    permission = fields.ReferenceField(Permission, required=True, reverse_delete_rule=CASCADE)

    meta = {
        'indexes': [
            {'fields': ['role', 'permission']}
        ]
    }


class User(Document):
    first_name = fields.StringField(max_length=30, required=False)
    last_name = fields.StringField(max_length=30, required=False)
    username = fields.StringField(max_length=30, required=False)
    email = fields.EmailField(max_length=30, required=False)
    password = fields.StringField(min_length=6, max_length=128, required=False)
    phone = fields.StringField(max_length=15, required=False)

    # Designates that this user has all permissions without explicitly assigning them.
    is_superuser = fields.BooleanField(default=False)

    USERNAME_FIELD = getattr(settings, 'USERNAME_FIELD', 'username')

    meta = {
        'indexes': [
            {'fields': ['username', 'email']}
        ]
    }

    def __unicode__(self):
        return self.username if self.username else self.email

    def save(self, *args, **kwargs):
        if not self.id:
            self.set_password(self.password)
        return super().save(*args, **kwargs)

    @property
    def full_name(self):
        return '{}{}'.format(self.first_name, self.last_name)

    @property
    def is_authenticated(self):
        """
        Always return True. This is a way to tell if the user has been
        authenticated in templates.
        """
        return True

    def set_password(self, raw_password):
        self.password = make_password(raw_password)
        return self

    def check_password(self, raw_password):
        return check_password(raw_password, self.password)

    def get_role_permissions(self, obj=None):
        """
        Returns a list of permission strings that this user has through his/her
        roles. This method queries all available auth backends. If an object
        is passed in, only permissions matching this object are returned.
        """
        permissions = set()
        for backend in auth.get_backends():
            if hasattr(backend, "get_role_permissions"):
                permissions.update(backend.get_role_permissions(self, obj))
        return permissions

    def get_all_permissions(self, obj=None):
        """
        Returns a list of permission strings that this user has through his/her
        roles. This method queries all available auth backends. If an object
        is passed in, only permissions matching this object are returned.
        """
        permissions = set()
        for backend in auth.get_backends():
            print(backend.__dict__)
            if hasattr(backend, "get_role_permissions"):
                permissions.update(backend.get_role_permissions(self, obj))
        return permissions

    def has_perm(self, perm, obj=None):
        """
        Returns True if the user has the specified permission. This method
        queries all available auth backends, but returns immediately if any
        backend returns True. Thus, a user who has permission from a single
        auth backend is assumed to have permission in general. If an object is
        provided, permissions for this specific object are checked.
        """
        # superusers have all permissions.
        if self.is_superuser:
            return True

        return _user_has_perm(self, perm, obj)

    def has_perms(self, perm_list, obj=None):
        """
        Returns True if the user has each of the specified permissions. If
        object is passed, it checks if the user has all required perms for this
        object.
        """
        for perm in perm_list:
            if not self.has_perm(perm, obj):
                return False
        return True

    @property
    def is_anonymous(self):
        """
        Always return False. This is a way of comparing User objects to
        anonymous users.
        """
        return False


class UserRole(Document):
    role = fields.ReferenceField(Role, required=True, reverse_delete_rule=CASCADE)
    user = fields.ReferenceField(User, required=True, reverse_delete_rule=CASCADE)

    meta = {
        'indexes': [
            {'fields': ['role', 'user']}
        ]
    }


class SocialProvider(Document):
    """
    This table keep settings for social providers for which this application would
    allow login for user.
    Admin can add/remove social providers from here to decide which social login
    to keep and which one to remove
    """
    name = fields.StringField(required=True)
    admin_email = fields.EmailField(required=True)

    """
    This field could save different type of values depends on provider
    it could be client_id and client_secret
    or it could be API_KEY or any other oAuth2.0 credentials of social site
    """
    configuration = fields.DictField(required=True)
    is_active = fields.BooleanField(default=True)

    def __unicode__(self):
        return '{} {}'.format(self.name, self.configuration)

    meta = {
        'indexes': [
            {'fields': ['name']}
        ]
    }
