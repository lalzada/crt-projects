from mongodbforms import DocumentForm
from django import forms
from bson import ObjectId
from configuration import models


class UserCreateForm(DocumentForm):

    username = forms.CharField(required=True)
    password = forms.CharField(required=True)
    roles = forms.MultipleChoiceField(
        choices=[(str(role.id), role.name) for role in models.Role.objects.all()]
    )

    class Meta:
        document = models.User
        fields = ['first_name', 'last_name', 'username', 'password', 'email', 'roles']

    def save(self):
        user = super().save(commit=True)
        roles = [ObjectId(role_id) for role_id in self.cleaned_data.get('roles')]
        for role in roles:
            models.UserRole.objects.create(role=role, user=user)
        return user

    def clean_roles(self):
        roles = self.cleaned_data.get('roles')
        if not roles:
            self.add_error('roles', 'This field is required')
        return roles


class UserEditForm(DocumentForm):
    roles = forms.MultipleChoiceField(
        choices=[(str(role.id), role.name) for role in models.Role.objects.all()]
    )

    class Meta:
        document = models.User
        fields = ['first_name', 'last_name', 'email', 'roles']

    def save(self):
        user = super().save(commit=True)
        roles = [ObjectId(role_id) for role_id in self.cleaned_data.get('roles')]
        models.UserRole.objects.filter(user=user.to_dbref()).delete()
        for role in roles:
            models.UserRole.objects.create(user=user.to_dbref(), role=role)

        return user


class RoleCreateForm(DocumentForm):
    name = forms.CharField(required=True)
    permissions = forms.MultipleChoiceField(
        choices=[(str(perm.id), perm.name) for perm in models.Permission.objects.all()]
    )

    class Meta:
        document = models.Role
        fields = ['name', 'permissions']

    def save(self):
        role = super().save(commit=True)
        permissions = [ObjectId(perm_id) for perm_id in self.cleaned_data.get('permissions')]
        for perm in permissions:
            models.RolePermission.objects.create(role=role.to_dbref(), permission=perm)

        return role

    def clean_permissions(self):
        permissions = self.cleaned_data.get('permissions')
        if not permissions:
            self.add_error('permissions', 'This field is required')
        return permissions


class RoleEditForm(DocumentForm):
    permissions = forms.MultipleChoiceField(
        choices=[(str(perm.id), perm.name) for perm in models.Permission.objects.all()]
    )

    class Meta:
        document = models.Role
        fields = ['name', 'permissions']

    def save(self):
        role = super().save(commit=True)
        permissions = [ObjectId(perm_id) for perm_id in self.cleaned_data.get('permissions')]
        models.RolePermission.objects.filter(role=role.to_dbref()).delete()
        for perm in permissions:
            models.RolePermission.objects.create(role=role.to_dbref(), permission=perm)

        return role
