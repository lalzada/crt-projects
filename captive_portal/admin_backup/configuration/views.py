from django.views import generic
from django.urls import reverse_lazy
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.mixins import PermissionRequiredMixin
from configuration import forms
from configuration import models


class RoleCreate(PermissionRequiredMixin, SuccessMessageMixin, generic.CreateView):
    template_name = 'configuration/role_create.html'
    form_class = forms.RoleCreateForm
    queryset = models.Role.objects.all()
    success_url = reverse_lazy('configuration:role-list')
    success_message = 'Role added successfully'
    permission_required = ('RoleCreate',)
    raise_exception = True


class RoleEdit(PermissionRequiredMixin, SuccessMessageMixin, generic.UpdateView):
    template_name = 'configuration/role_edit.html'
    form_class = forms.RoleEditForm
    queryset = models.Role.objects.all()
    success_url = reverse_lazy('configuration:role-list')
    success_message = 'Role updated successfully.'
    permission_required = ('RoleEdit',)
    raise_exception = True

    def get_context_data(self, **kwargs):
        role = self.get_object()
        kwargs = super().get_context_data(**kwargs)
        role_permissions = models.RolePermission.objects.filter(role=role.to_dbref())
        kwargs.get('form').fields['permissions'].initial = [str(perm.permission.id) for perm in role_permissions]
        return kwargs


class RoleList(PermissionRequiredMixin, generic.ListView):
    template_name = 'configuration/role_list.html'
    queryset = models.Role.objects.all()
    context_object_name = 'roles'
    permission_required = ('RoleList',)
    raise_exception = True

    def get_context_data(self, *args, **kwargs):
        kwargs = super().get_context_data(*args, **kwargs)
        roles = []
        for role in kwargs.get('roles'):
            r = {
                'id': role.id,
                'name': role.name,
                'permissions': [perm.permission.name for perm in models.RolePermission.objects.filter(role=role.to_dbref())]
            }
            roles.append(r)

        kwargs['roles'] = roles
        return kwargs


class RoleDelete(PermissionRequiredMixin, generic.DeleteView):
    queryset = models.Role.objects.all()
    success_url = reverse_lazy('configuration:role-list')
    success_message = 'Role deleted successfully.'
    permission_required = ('RoleDelete',)
    raise_exception = True

    def delete(self, request, *args, **kwargs):
        response = super().delete(request, *args, **kwargs)
        messages.success(request, self.success_message)
        return response


class UserCreate(PermissionRequiredMixin, SuccessMessageMixin, generic.CreateView):
    template_name = 'configuration/user_create.html'
    form_class = forms.UserCreateForm
    queryset = models.User.objects.all()
    success_url = reverse_lazy('configuration:user-list')
    success_message = 'User added successfully'
    permission_required = ('UserCreate',)
    raise_exception = True


class UserList(PermissionRequiredMixin, generic.ListView):
    template_name = 'configuration/user_list.html'
    queryset = models.User.objects.filter(is_superuser=False)
    context_object_name = 'users'
    permission_required = ('UserList',)
    raise_exception = True

    def get_context_data(self, *args, **kwargs):
        kwargs = super().get_context_data(*args, **kwargs)
        users = []
        for user in kwargs.get('users'):
            r = {
                'id': user.id,
                'first_name': user.first_name,
                'last_name': user.last_name,
                'username': user.username,
                'roles': [user_role.role.name for user_role in models.UserRole.objects.filter(user=user.to_dbref())]
            }
            users.append(r)

        kwargs['users'] = users
        return kwargs


class UserEdit(PermissionRequiredMixin, SuccessMessageMixin, generic.UpdateView):
    template_name = 'configuration/user_edit.html'
    form_class = forms.UserEditForm
    queryset = models.User.objects.all()
    success_url = reverse_lazy('configuration:user-list')
    success_message = 'User updated successfully.'
    permission_required = ('UserEdit',)
    raise_exception = True

    def get_context_data(self, **kwargs):
        user = self.get_object()
        kwargs = super().get_context_data(**kwargs)
        user_roles = models.UserRole.objects.filter(user=user.to_dbref())
        kwargs.get('form').fields['roles'].initial = [str(user_role.role.id) for user_role in user_roles]
        return kwargs


class UserDelete(PermissionRequiredMixin, generic.DeleteView):
    queryset = models.User.objects.all()
    success_url = reverse_lazy('configuration:user-list')
    success_message = 'User deleted successfully.'
    permission_required = ('UserDelete',)
    raise_exception = True

    def delete(self, request, *args, **kwargs):
        response = super().delete(request, *args, **kwargs)
        messages.success(request, self.success_message)
        return response
