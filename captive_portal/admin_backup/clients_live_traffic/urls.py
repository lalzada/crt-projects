from django.urls import path
from clients_live_traffic import views

app_name = 'wifi_clients_data'

urlpatterns = [
    path('wifi-clients-data', views.WifiClientsData.as_view(), name='wifi_clients_data'),
]
