from django.views import generic
from django.conf import settings


class WifiClientsData(generic.TemplateView):
    template_name = 'clients_live_traffic/wifi_clients_data.html'

    def get_context_data(self, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs['wifi_clients_data_url'] = settings.WIFI_CLIENTS_DATA_URL
        return kwargs

