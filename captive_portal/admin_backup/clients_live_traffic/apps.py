from django.apps import AppConfig


class ClientsLiveTrafficConfig(AppConfig):
    name = 'clients_live_traffic'
