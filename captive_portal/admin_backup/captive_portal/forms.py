from mongodbforms import DocumentForm
from django import forms
from captive_portal import models


class AllowedSocialLoginForm(forms.Form):
    id = forms.CharField(widget=forms.HiddenInput())
    name = forms.CharField(required=False)
    is_active = forms.BooleanField(required=False)

    class Meta:
        fields = ['id', 'name', 'is_active']


class WifiLoginTimeoutForm(DocumentForm):

    class Meta:
        document = models.Settings
        fields = ['login_timeout']


class PackageForm(DocumentForm):
    class Meta:
        document = models.Package
        fields = ['name', 'data_limit', 'activation_days', 'expiration_days']


class VoucherForm(DocumentForm):

    class Meta:
        document = models.Voucher
        fields = ['name', 'code', 'package']
