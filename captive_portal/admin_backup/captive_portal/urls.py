from django.urls import path
from captive_portal import views
from django.contrib.auth.context_processors import auth


app_name = 'captive_portal'

urlpatterns = [
    path('', views.CaptivePortalOverview.as_view(), name='overview'),

    path('settings', views.Settings.as_view(), name='settings'),

    path('vouchers', views.VoucherList.as_view(), name='voucher-list'),
    path('vouchers/create', views.VoucherCreate.as_view(), name='voucher-create'),
    path('voucher/edit/<str:pk>', views.VoucherEdit.as_view(), name='voucher-edit'),
    path('voucher/delete/<str:pk>', views.VoucherDelete.as_view(), name='voucher-delete'),

    path('package/create', views.PackageCreate.as_view(), name='package-create'),
    path('package/edit/<str:pk>', views.PackageEdit.as_view(), name='package-edit'),
    path('package/delete/<str:pk>', views.PackageDelete.as_view(), name='package-delete'),

]
