from django.views import generic
from django.urls import reverse_lazy
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from functools import reduce
from django.forms import formset_factory
from base.utils import convert_size
from captive_portal import forms
from captive_portal import models
from configuration.models import SocialProvider


class CaptivePortalOverview(PermissionRequiredMixin, generic.TemplateView):
    template_name = 'captive_portal/overview.html'
    permission_required = ('CaptivePortalOverview',)
    raise_exception = True

    def get_context_data(self, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs['users_count'] = models.WifiUser.objects.count()
        kwargs['guests_count'] = models.WifiGuest.objects.count()
        active_guests = models.WifiGuest.objects.filter(is_valid=True)
        # aggregate i.e sum() doesn't work in MongoDB 2.0, so traverse each record and sum it
        data_usage = reduce(
            lambda s, guest: s + guest.downloadTotal + guest.uploadTotal,
            active_guests, 0
        )
        kwargs['data_usage'] = convert_size(data_usage)
        return kwargs


class Settings(PermissionRequiredMixin, SuccessMessageMixin, generic.FormView):
    template_name = 'captive_portal/settings.html'
    form_class = forms.AllowedSocialLoginForm
    success_url = reverse_lazy('captive_portal:settings')
    success_message = 'Settings updated'
    object = None
    permission_required = ('Settings',)
    raise_exception = True

    @staticmethod
    def get_object():
        if models.Settings.objects.count() < 1:
            return models.Settings.objects.create()

        return models.Settings.objects.first()

    def get_context_data(self, **kwargs):
        social_providers = SocialProvider.objects.all()
        kwargs = super().get_context_data(**kwargs)
        if 'social_login_formset' not in kwargs:
            initial_data = [{
                            'id': p.id, 'is_active': p.is_active, 'name': p.name
                            } for p in social_providers]

            formset = formset_factory(
                form=self.form_class,
                min_num=social_providers.count(),
                max_num=social_providers.count(),
                extra=0
            )

            kwargs['social_login_formset'] = formset(initial=initial_data)

        if 'wifi_timeout_form' not in kwargs:
            kwargs['wifi_timeout_form'] = forms.WifiLoginTimeoutForm(instance=self.get_object())
        return kwargs

    def post(self, request):
        wifi_timeout_form = forms.WifiLoginTimeoutForm(data=request.POST, instance=self.get_object())

        if request.POST['form'] == 'social-login-form':
            formset = formset_factory(form=self.form_class)(data=request.POST)
            if formset.is_valid():
                for form in formset:
                    data = form.cleaned_data
                    SocialProvider.objects.get(id=data.get('id')).update(is_active=data.get('is_active'))

                return self.form_valid(formset)

        if request.POST['form'] == 'wifi-timeout-form':
            if wifi_timeout_form.is_valid():
                wifi_timeout_form.save()
                return self.form_valid(wifi_timeout_form)
            else:
                return self.render_to_response(
                    self.get_context_data(wifi_timeout_form=wifi_timeout_form))


class VoucherList(PermissionRequiredMixin, generic.ListView):
    template_name = 'captive_portal/voucher_list.html'
    queryset = models.Voucher.objects.all()
    context_object_name = 'vouchers'
    permission_required = ('VoucherList',)
    raise_exception = True

    def get_context_data(self, *args, **kwargs):
        kwargs = super(VoucherList, self).get_context_data(*args, **kwargs)
        kwargs['packages'] = models.Package.objects.all()
        return kwargs


class VoucherCreate(PermissionRequiredMixin, SuccessMessageMixin, generic.CreateView):
    template_name = 'captive_portal/voucher_create.html'
    model = models.Voucher
    form_class = forms.VoucherForm
    success_url = reverse_lazy('captive_portal:voucher-list')
    success_message = 'Voucher added successfully.'
    permission_required = ('VoucherCreate',)
    raise_exception = True


class VoucherEdit(PermissionRequiredMixin, SuccessMessageMixin, generic.UpdateView):
    template_name = 'captive_portal/voucher_edit.html'
    queryset = models.Voucher.objects.all()
    form_class = forms.VoucherForm
    success_url = reverse_lazy('captive_portal:voucher-list')
    success_message = 'Voucher updated successfully.'
    permission_required = ('VoucherEdit',)
    raise_exception = True


class VoucherDelete(PermissionRequiredMixin, generic.DeleteView):
    queryset = models.Voucher.objects.all()
    success_url = reverse_lazy('captive_portal:voucher-list')
    success_message = 'Voucher deleted successfully.'
    permission_required = ('VoucherDelete',)
    raise_exception = True

    def delete(self, request, *args, **kwargs):
        response = super().delete(request, *args, **kwargs)
        messages.success(request, self.success_message)
        return response


class PackageCreate(PermissionRequiredMixin, SuccessMessageMixin, generic.CreateView):
    template_name = 'captive_portal/package_create.html'
    model = models.Package
    form_class = forms.PackageForm
    success_url = reverse_lazy('captive_portal:voucher-list')
    success_message = 'Package added successfully.'
    permission_required = ('PackageCreate',)
    raise_exception = True


class PackageEdit(PermissionRequiredMixin, SuccessMessageMixin, generic.UpdateView):
    template_name = 'captive_portal/package_edit.html'
    queryset = models.Package.objects.all()
    form_class = forms.PackageForm
    success_url = reverse_lazy('captive_portal:voucher-list')
    success_message = 'Package updated successfully.'
    permission_required = ('PackageEdit',)
    raise_exception = True


class PackageDelete(PermissionRequiredMixin, generic.DeleteView):
    queryset = models.Package.objects.all()
    success_url = reverse_lazy('captive_portal:voucher-list')
    success_message = 'Package deleted successfully.'
    permission_required = ('PackageDelete',)
    raise_exception = True

    def delete(self, request, *args, **kwargs):
        response = super().delete(request, *args, **kwargs)
        messages.success(request, self.success_message)
        return response



