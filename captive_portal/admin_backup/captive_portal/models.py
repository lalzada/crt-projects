from mongoengine import fields, Document, DynamicDocument, CASCADE
from django.utils import timezone
from django.conf import settings
from configuration.models import User
from dashboard.models import Device


class Settings(Document):
    # in minutes
    login_timeout = fields.IntField(min_value=1, default=1, required=True)


class Package(Document):
    name = fields.StringField(required=True)
    data_limit = fields.StringField(required=True)
    activation_days = fields.IntField(min_value=1, required=True)
    expiration_days = fields.IntField(min_value=1, required=True)

    def __unicode__(self):
        return self.name if self.name else 'N/A'


class Voucher(Document):
    STATUS_CHOICES = (
        ('purchased', 'Purchased'),
        ('active', 'Active'),
        ('expired', 'Expired'),
        ('disabled', 'Disabled'),
        ('fullfilled', 'Fullfilled')
    )
    name = fields.StringField(required=True)
    code = fields.StringField(required=True)
    creation_date = fields.DateTimeField(default=timezone.now)
    expiration_date = fields.DateTimeField(default=timezone.now)
    activation_date = fields.DateTimeField(default=timezone.now)
    activation_expiration_date = fields.DateTimeField(default=timezone.now)
    # in GBs
    data_limit = fields.IntField(min_value=1)
    status = fields.StringField(choices=STATUS_CHOICES)
    total_data_usage = fields.StringField()

    package = fields.ReferenceField(Package, reverse_delete_rule=CASCADE, required=True)


class SessionHistory(Document):
    start_datetime = fields.DateTimeField(default=timezone.now)
    end_datetime = fields.DateTimeField(default=timezone.now)
    data_usage = fields.StringField()
    voucher = fields.ReferenceField(Voucher, reverse_delete_rule=CASCADE)
    device = fields.ReferenceField(Device, reverse_delete_rule=CASCADE)
    user = fields.ReferenceField(User, reverse_delete_rule=CASCADE)


# These are Collections for existing database just to read data from WIFI Database
class WifiUser(DynamicDocument):
    meta = {
        'db_alias': settings.WIFI_DB_ALIAS,
        'collection': 'client',
    }


class WifiGuest(DynamicDocument):
    meta = {
        'db_alias': settings.WIFI_DB_ALIAS,
        'collection': 'guest',
    }
