from django.contrib import auth
from django.contrib.auth.backends import ModelBackend
from configuration.models import User, RolePermission, UserRole


class Authentication(ModelBackend):
    """
    Authenticate using MongoEngine and settings.MONGO_USER_DOCUMENT
    Authenticate user against username and password
    """

    def authenticate(self, request=None, username=None, password=None, **kwargs):
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            return None

        if user:
            if password and user.check_password(password):
                backend = auth.get_backends()[0]
                user.backend = "%s.%s" % (backend.__module__, backend.__class__.__name__)
                return user

        return None

    def user_can_authenticate(self, user):
        """
        Reject users with is_active=False. Custom user models that don't have
        that attribute are allowed.
        """
        return True

    @staticmethod
    def _get_role_permissions(user_obj):
        user_roles = UserRole.objects.filter(user=user_obj.to_dbref()).values_list('role')
        user_role_permissions = RolePermission.objects.filter(role__in=user_roles).values_list('permission')
        return user_role_permissions

    def _get_permissions(self, user_obj, obj, from_name):
        """
        Return the permissions of `user_obj` from `from_name`. `from_name` can
        be either "group" or "user" to return permissions from
        `_get_group_permissions` or `_get_user_permissions` respectively.
        """
        if user_obj.is_anonymous or obj is not None:
            return set()

        perm_cache_name = '_%s_perm_cache' % from_name
        if not hasattr(user_obj, perm_cache_name):
            perms = getattr(self, '_get_%s_permissions' % from_name)(user_obj)
            setattr(user_obj, perm_cache_name, {perm.name.lower() for perm in perms})
        return getattr(user_obj, perm_cache_name)

    def get_role_permissions(self, user_obj, obj=None):
        """
        Return a set of permission strings the user `user_obj` has from the
        groups they belong.
        """
        return self._get_permissions(user_obj, obj, 'role')

    def get_all_permissions(self, user_obj, obj=None):
        if user_obj.is_anonymous or obj is not None:
            return set()
        if not hasattr(user_obj, '_perm_cache'):
            user_obj._perm_cache = set()
            user_obj._perm_cache.update(self.get_role_permissions(user_obj))
        return user_obj._perm_cache

    def has_perm(self, user_obj, perm, obj=None):
        return perm.lower() in self.get_all_permissions(user_obj, obj)

    def get_user(self, user_id):
        try:
            return User.objects.get(id=user_id)
        except User.DoesNotExist:
            return None
