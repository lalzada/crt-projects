from .base import *
import mongoengine

SECRET_KEY = 'ns^489a&sb$u(&l8pcutz3a0s@n)4b1c)gs(qf5q0q@6rrb7za'

DEBUG = True

ALLOWED_HOSTS = ['*']

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.0/howto/static-files/

STATIC_ROOT = os.path.join(BASE_DIR, '..', 'static')

STATIC_URL = '/static/'

mongoengine.connect(db='captive-portal-api')






