from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import authenticate
from django import forms


class LoginForm(AuthenticationForm):

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username and password:
            user = authenticate(self.request, username=username, password=password)
            if not user:
                raise forms.ValidationError(
                    self.error_messages['invalid_login'],
                    code='invalid_login',
                    params={'username': self.username_field.verbose_name},
                )

            if not self.user_cache:
                self.user_cache = user

        return self.cleaned_data

