from django.contrib.auth.views import LoginView, LogoutView
from django.shortcuts import redirect
from django.urls import reverse_lazy
from user import forms
from base.authentication import login


class UserLogin(LoginView):
    template_name = 'user/login.html'
    redirect_authenticated_user = True
    authentication_form = forms.LoginForm

    def form_valid(self, form):
        login(self.request, form.get_user())
        return redirect(self.get_success_url())


class UserLogout(LogoutView):
    next_page = reverse_lazy('user:login')






