from mongoengine import fields, Document, EmbeddedDocument, CASCADE
from configuration.models import User


class Browser(EmbeddedDocument):
    name = fields.StringField()

    def __str__(self):
        return self.name


class Device(Document):
    mac_address = fields.StringField(required=True)
    type = fields.StringField(default='other')
    operating_system = fields.StringField(default='other')
    model = fields.StringField(default='other')
    browsers = fields.EmbeddedDocumentListField(Browser)
    user = fields.ReferenceField(User, reverse_delete_rule=CASCADE)

    meta = {
        'indexes': [
            {'fields': ['user', 'mac_address', 'type']}
        ]
    }
