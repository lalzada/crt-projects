from django.views import generic
from django.contrib.auth.mixins import PermissionRequiredMixin
from .models import Device
from collections import Counter


class Dashboard(PermissionRequiredMixin, generic.TemplateView):
    template_name = 'dashboard/index.html'
    permission_required = ('dashboard',)
    raise_exception = True

    def get_context_data(self, **kwargs):
        import json
        context = super().get_context_data(**kwargs)
        devices = Device.objects.aggregate({"$group": {"_id": "$type", "value": {"$sum": 1},
                                                       "name": {"$first": "$type"}}})

        device_list = [
            {"name": device.get('name').capitalize() if device.get('name') else 'other', "value": device['value']}
            for device in devices
        ]
        context['device_list'] = json.dumps(device_list)

        device_models = Device.objects.aggregate({"$group": {"_id": "$model", "value": {"$sum": 1},
                                                             "name": {"$first": "$model"}}})

        device_models = [
            {"name": model.get('name').capitalize() if model.get('name') else 'other', "value": model['value']}
            for model in device_models
        ]
        context['device_models'] = json.dumps(device_models)

        os_list = Device.objects.aggregate({"$group": {"_id": "$operating_system", "value": {"$sum": 1},
                                                       "name": {"$first": "$operating_system"}}})

        os_list = [
            {"name": os.get('name').capitalize() if os.get('name') else 'other', "value": os['value']}
            for os in os_list
        ]
        context['os_list'] = json.dumps(os_list)

        browsers = Device.objects().all()
        browser_list = [browser.name for device in browsers for browser in device.browsers]
        browser_counts = Counter(browser_list)
        browser_list = [{'name': key, 'value': value} for key, value in dict(browser_counts).items()]
        context['browser_list'] = json.dumps(browser_list)

        return context

