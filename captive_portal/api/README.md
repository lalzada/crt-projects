#### Captive Portal API

###### Tools
1. Python 3.5
2. Django (2.0)
3. Django REST Framework 3.7.7
4. [Django Rest Framework Mongoengine 3.3.0 (Mongoengine support for Django Rest Framework)](https://github.com/umutbozkurt/django-rest-framework-mongoengine)
5. Django REST Swagger 2.1.2
6. [MongoEngine (Django ORM for MongoDB) 0.9](https://github.com/MongoEngine/mongoengine/tree/v0.9.0)

###### Setup

Install dependencies
```
pip3 install -r requirements/local.txt # /staging.txt, /production.txt
```

Run project
```
./manage.py runserver --settings=base.settings.local # .staging, .production
```

Settings are split into multiple files based on local, staging and production
