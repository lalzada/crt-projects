from mongoengine.django.mongo_auth.models import get_user_document
from rest_framework.authentication import BaseAuthentication
from rest_framework.exceptions import AuthenticationFailed


class IsUserLoggedIn(BaseAuthentication):
    def authenticate(self, request):
        logged_in_users = get_user_document().objects.filter(is_logged_in=True)
        if logged_in_users.count() > 0:
            user = logged_in_users.first()
            return user, None

        raise AuthenticationFailed('A user must be logged in to create video')

