from .base import *
import mongoengine

SECRET_KEY = 'ns^489a&sb$u(&l8pcutz3a0s@n)4b1c)gs(qf5q0q@6rrb7za'

DEBUG = True

ALLOWED_HOSTS = ['*']


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.0/howto/static-files/

STATIC_ROOT = os.path.join(BASE_DIR, '..', 'static')

STATIC_URL = '/static/'

mongoengine.connect(db='captive-portal-api')

INSTALLED_APPS += ['django.contrib.staticfiles']

CORS_ORIGIN_ALLOW_ALL = True

CONTROLLER_SERVER_USER = 'portaluser'
CONTROLLER_SERVER_PASSWORD = 'cresset123'
CONTROLLER_SERVER_URL = 'https://172.16.190.10:8043'


LOGGING['handlers'] = {
        'file': {
            'level': 'INFO',
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'filename': os.path.join('/', 'vagrant', 'api.log'),
            'when': 'D',  # this specifies the interval
            'interval': 1,  # defaults to 1, only necessary for other values
            'backupCount': 10,  # how many backup file to keep, 10 days
            'formatter': 'simple',
        },
        'console': {
            'class': 'logging.StreamHandler',
        },

    }

LOGGING['loggers']['django'] = {
                            'handlers': ['console'],
                            'level': 'INFO',
                            'propagate': True,
                        }







