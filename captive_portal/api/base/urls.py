from django.urls import path, include
from rest_framework_swagger.views import get_swagger_view
from django.conf.urls.static import static
from django.conf import settings
import os

urlpatterns = [
    path('user/', include('user.urls', namespace='user'))
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

baseUrl = '/'

if os.environ['DJANGO_SETTINGS_MODULE'] == 'base.settings.staging':
    baseUrl = '/captive-portal-api/'

schema_view = get_swagger_view(
    title='Captive Portal API', patterns=urlpatterns, url=baseUrl)

urlpatterns += [path('', schema_view)]
