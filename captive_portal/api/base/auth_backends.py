from django.contrib import auth
from mongoengine.django.auth import MongoEngineBackend


class CustomAuthentication(MongoEngineBackend):
    """
    Authenticate using MongoEngine and settings.MONGO_USER_DOCUMENT
    Authenticate user against username and password
    """

    def authenticate(self, request=None, username=None, password=None):
        try:
            user = self.user_document.objects.get(username=username)
        except self.user_document.DoesNotExist:
            return None

        if user:
            if password and user.check_password(password):
                backend = auth.get_backends()[0]
                user.backend = "%s.%s" % (backend.__module__, backend.__class__.__name__)
                return user
        return None


class SocialAuthentication(MongoEngineBackend):
    """
    Authenticate using MongoEngine and settings.MONGO_USER_DOCUMENT
    Authenticate user against email
    """

    def authenticate(self, request=None, email=None):
        try:
            user = self.user_document.objects.get(email=email)
            return user
        except self.user_document.DoesNotExist:
            return None

