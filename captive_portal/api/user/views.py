from mongoengine.django.mongo_auth.models import get_user_document
from rest_framework import status
from rest_framework.response import Response
from rest_framework_mongoengine import generics
from django.conf import settings
import requests
import logging
from .models import Provider, Settings

from user import serializers

UserModel = get_user_document()
log = logging.getLogger('custom')


class Login(generics.GenericAPIView):
    serializer_class = serializers.CustomLoginSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.login_user()
        if controller_server_login(request):
            response = {'success': True}
        else:
            response = {'success': False}

        return Response(response, status=status.HTTP_200_OK)


class Register(generics.CreateAPIView):
    serializer_class = serializers.CustomRegisterSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        current_user = UserModel.objects.get(username=serializer.validated_data.get('username'))
        return Response(current_user.to_json(), status=status.HTTP_200_OK)


class SocialLogin(generics.GenericAPIView):
    serializer_class = serializers.SocialLoginSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.login_user()
        if controller_server_login(request):
            response = {'success': True}
        else:
            response = {'success': False}
        return Response(response, status=status.HTTP_200_OK)


class AllowedSocialLogin(generics.ListAPIView):
    serializer_class = serializers.AllowedSocialLoginSerializer
    queryset = Provider.objects.all()


def controller_server_login(request):
    success = False
    client_data = request.GET.copy()
    try:
        login_timeout = Settings.objects.first().login_timeout
        client_data['time'] = login_timeout * 60
    except:
        client_data['time'] = 3600

    data = {
        'name': settings.CONTROLLER_SERVER_USER,
        'password': settings.CONTROLLER_SERVER_PASSWORD
    }
    with requests.Session() as s:
        s.verify = False

        # Login to controller server with username/password
        response = s.post(
            '{}/login'.format(settings.CONTROLLER_SERVER_URL),
            data=data
        )

        log.info(response.json())
        print(response.json())

        # If login successfully. Login current user with mac address and other details
        if response.json()['success']:
            success = True
            response = s.post(
                '{0}/extportal/{1}/auth'.format(settings.CONTROLLER_SERVER_URL, client_data.get('site')),
                data=client_data
            )

            log.info(response.json())
            print(response.json())

        # Logout from controller server
        response = s.post('{}/logout'.format(settings.CONTROLLER_SERVER_URL))
        logging.info(response.json())
        print(response.json())

    return success

