from mongoengine.django.mongo_auth.models import get_user_document
from rest_framework.exceptions import AuthenticationFailed, ValidationError
from rest_framework_mongoengine.serializers import DocumentSerializer
from rest_framework import serializers
from django.contrib.auth import authenticate
from user_agents import parse
from dict2xml import dict2xml
import xmltodict
from .models import Provider, Device, UserProvider, ProviderSettings

UserModel = get_user_document()

fields_mapper = {
    'string': serializers.CharField,
    'email': serializers.EmailField,
    'integer': serializers.IntegerField,
    'boolean': serializers.BooleanField,
    'date': serializers.DateField,
    'datetime': serializers.DateTimeField
}

attr_mapper = {
    'True': True,
    'true': True,
    'False': False,
    'false': False,
}


class UserDeviceMixin(object):

    @staticmethod
    def get_device_type(user_agent):
        if user_agent.is_mobile:
            return 'mobile'
        if user_agent.is_tablet:
            return 'tablet'
        if user_agent.is_pc:
            return 'pc'
        if user_agent.is_bot:
            return 'bot'

    def add_user_device(self, user):
        data = self.initial_data
        user_agent = parse(self.context['request'].META.get('HTTP_USER_AGENT'))
        try:
            device = Device.objects.get(user=user.to_dbref(), mac_address=data.get('mac_address'))
            device.operating_system = user_agent.os.family
            device.type = self.get_device_type(user_agent)
            device.browser = user_agent.browser.family
            device.model = user_agent.device.family
            device.save()

        except Device.DoesNotExist:
            device = Device.objects.create(
                mac_address=data.get('mac_address'),
                operating_system=user_agent.os.family,
                type=self.get_device_type(user_agent),
                model=user_agent.device.family,
                user=user.to_dbref(),
                browser=user_agent.browser.family
            )
            device.save()


class CustomLoginSerializer(UserDeviceMixin, DocumentSerializer):

    username = serializers.CharField()
    password = serializers.CharField()
    mac_address = serializers.CharField()

    class Meta:
        model = UserModel
        fields = ('username', 'password', 'mac_address')

    def validate(self, data):
        super().validate(data)
        user = authenticate(
            request=self.context['request'],
            username=data.get('username'),
            password=data.get('password')
        )
        if user is None:
            raise AuthenticationFailed(detail='Invalid username or password')
        else:
            self.add_user_device(user)

        return data


class CustomRegisterSerializer(DocumentSerializer):

    first_name = serializers.CharField(allow_blank=True)
    last_name = serializers.CharField(allow_blank=True)
    username = serializers.CharField()
    password = serializers.CharField()
    phone = serializers.CharField()
    email = serializers.EmailField()

    class Meta:
        model = UserModel
        fields = ('username', 'password', 'phone', 'email', 'first_name', 'last_name')

    @staticmethod
    def validate_username(value):
        try:
            UserModel.objects.get(username=value)
            raise ValidationError(
                detail='Someone is already registered using provided username')
        except UserModel.DoesNotExist:
            pass
        return value


class SocialLoginSerializer(UserDeviceMixin, serializers.Serializer):

    PROVIDER_CHOICES = Provider.objects.values_list('name')

    first_name = serializers.CharField(required=False, allow_blank=True)
    last_name = serializers.CharField(required=False, allow_blank=True)
    email = serializers.EmailField()
    mac_address = serializers.CharField()
    provider_name = serializers.ChoiceField(
        choices=PROVIDER_CHOICES,
        error_messages={
            'invalid_choice': 'provider_name must be one of the these. {}'.format(", ".join(PROVIDER_CHOICES))
        })

    class Meta:
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        self.auth_fields = None
        self.auth_data = None
        if kwargs.get('data'):
            self.setup_dynamic_fields()

        super().__init__(*args, **kwargs)

    def setup_dynamic_fields(self):
        data = self._kwargs.get('data')
        try:
            provider = Provider.objects.get(name__iexact=data.get('provider_name'))
        except Provider.DoesNotExist:
            msg = 'provider_name must be one of the these. {}'.format(", ".join(SocialLoginSerializer.PROVIDER_CHOICES))
            raise serializers.ValidationError({'provider_name': [msg]})

        provider_settings = ProviderSettings.objects.get(provider=provider.to_dbref())
        self.auth_fields = xmltodict.parse(provider_settings.auth_fields.strip())
        for field_name, field_attrs in self.auth_fields['fields'].items():
            field = fields_mapper.get(field_attrs.pop('type'))
            attrs = {}
            if field_attrs.get('required'):
                attrs.update({'required': attr_mapper.get(field_attrs.get('required'))})

            if field_attrs.get('max_length'):
                attrs.update({'max_length': int(field_attrs.get('max_length'))})

            if field_attrs.get('min_length'):
                attrs.update({'min_length': int(field_attrs.get('min_length'))})

            if field_attrs.get('allow_null'):
                attrs.update({'allow_null': attr_mapper.get(field_attrs.get('allow_null'))})

            self.fields[field_name] = field(**attrs)

    def login_user(self):
        data = self.validated_data
        user = authenticate(request=self.context['request'], email=data.get('email'))
        self.auth_data = {field_name: data.get(field_name) for field_name in self.auth_fields['fields']}
        self.auth_data = dict2xml(self.auth_data)

        """
        in case user is trying to login with any social site i.e Facebook, Google
        Twitter, we are first checking if user already exist with given email and 
        provider_name (twitter, facebook etc). If exist then do nothing. 
        If user doesn't exist, create a new social user
        """
        if not user:
            user = self.create_user()
        else:
            self.create_or_update_profile(user)

        self.add_user_device(user)
        return data

    def create_user(self):
        data = self.validated_data
        user = UserModel(
            first_name=data.get('first_name'),
            last_name=data.get('last_name'),
            email=data.get('email'))
        user.save()
        self.create_social_profile(user)
        return user

    def create_social_profile(self, user):
        data = self.validated_data
        provider = Provider.objects.get(name__iexact=data.get('provider_name').lower())
        UserProvider.objects.create(
            user=user.to_dbref(),
            provider=provider.to_dbref(),
            auth_data=self.auth_data
        )

    def create_or_update_profile(self, user):
        data = self.validated_data
        try:
            provider = Provider.objects.get(name__iexact=data.get('provider_name').lower())
            user_provider = UserProvider.objects.get(user=user.to_dbref(), provider=provider.to_dbref())
            user_provider.update(auth_data=self.auth_data)

        except UserProvider.DoesNotExist:
            self.create_social_profile(user)


class AllowedSocialLoginSerializer(DocumentSerializer):
    class Meta:
        model = Provider
        fields = '__all__'

