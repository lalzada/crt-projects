from django.urls import path

from user import views

app_name = 'user'

urlpatterns = [
    path('login', views.Login.as_view(), name='login'),
    path('register', views.Register.as_view(), name='register'),
    path('login/social', views.SocialLogin.as_view(), name='social-login'),
    path('allowed_social_login/list', views.AllowedSocialLogin.as_view(), name='social-login-list')
]
