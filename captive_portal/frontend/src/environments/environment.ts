// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAUdgaSli9zJH0K2dL6OTowvLadwD-pzCE",
    authDomain: "social-auth-app-187412.firebaseapp.com",
    databaseURL: "https://social-auth-app-187412.firebaseio.com",
    projectId: "social-auth-app-187412",
    storageBucket: "social-auth-app-187412.appspot.com",
    messagingSenderId: "714395160247"
  },
  api_url: 'http://localhost:8083'
};
