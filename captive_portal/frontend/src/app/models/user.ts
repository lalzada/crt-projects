export class UserModel {
    constructor(
        public email?: string,
        public mac_address: string = '',
        public username?: string,
        public phone?: string,
        public password?: string,
        public first_name?: string,
        public last_name?: string,
      ){}
}
