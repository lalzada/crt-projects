import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthComponent } from './../components/auth/auth.component';
import { PrivacyComponent } from './../components/privacy/privacy.component';
import { AuthService } from './../services/auth.service';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'privacy', component: PrivacyComponent },
  { path: ':authType', component: AuthComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class RoutingModule { }

