import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  authType: string = ''; // current active route. login or register
  queryStringData: object = {};

  constructor(
    private location: Location,
    private route: ActivatedRoute,
    private router: Router,
  ) {  }

  ngOnInit() {
    this.route.params.forEach(params => this.authType = params['authType']);
    this.route.queryParams.subscribe(params => this.queryStringData = params)
  }

  goToLogin = () => {
    this.router.navigate(['/login'], { queryParams: this.queryStringData });
    this.authType = 'login';
  }
 
  goToRegister = () => {
    this.router.navigate(['/register'], { queryParams: this.queryStringData });
    this.authType = 'login';
    this.authType = 'register';
  }

}
