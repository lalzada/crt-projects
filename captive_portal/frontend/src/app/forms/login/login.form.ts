import { Component, OnInit, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FacebookService, LoginResponse, InitParams, LoginOptions } from 'ngx-facebook';

import { UserModel } from '../../models/user';
import { AuthService } from './../../services/auth.service';
import { SettingsService } from './../../services/settings.service';
import { provideForRootGuard } from '@angular/router/src/router_module';
import { element } from 'protractor';
import { resetFakeAsyncZone } from '@angular/core/testing';


@Component({
  selector: 'login-form',
  templateUrl: './login.form.html',
  styleUrls: ['./login.form.scss'],
})
export class LoginForm {

  model = new UserModel();
  queryStringParams: string = '';
  loading: boolean = false;
  success: string = '';
  error: string = '';
  allowedSocialLogins: object[] = [];
  formErrors: any = {};
  options: LoginOptions = {
    scope: 'public_profile,email',
    return_scopes: true,
  };

  constructor(
    private authService: AuthService,
    private settingsService: SettingsService,
    private fb: FacebookService,
    private router: Router,
    private route: ActivatedRoute,
    private zone: NgZone
  ){
    
  }

  ngOnInit() {

    this.settingsService.allowedSocialLogins().subscribe(
      data => { 
        this.allowedSocialLogins = data;
        this.allowedSocialLogins.forEach(elem => {
            if (elem['name'].toLowerCase() === 'facebook'){
              const initParams: InitParams = {
                appId: elem['configuration']['app_id'],
                xfbml: true,
                version: elem['configuration']['version'],
                status: false,
              };
          
              this.fb.init(initParams);
            }
        });
       },
      error => console.log(error)
    )

    // http://www.tp-link.com/us/faq-916.html

    this.route.queryParams.subscribe(params => {
      this.model.mac_address = params['cid'];
      console.log(params);
      this.queryStringParams = Object.keys(params).map(key => key + '=' + params[key]).join('&');
    });
  }

  login() {  
    this.loading = true;
    this.authService.login(this.model, this.queryStringParams).subscribe(
      data => {
        this.loading = false;
        this.error = '';
        this.formErrors = {};
        this.success = data.success;
      },
      error => {
        this.success = '';
        this.formErrors = {};
        this.loading = false;

        if (error.status === 400){
            this.formErrors = JSON.parse(error._body);
            
            if (Object.keys(this.formErrors).length === 1 && this.formErrors.mac_address){
              this.error = JSON.stringify(this.formErrors);
            }
        } 
        
        else if (error.status === 403){
          this.error = JSON.parse(error._body).detail;
        }

        else{
          this.error = JSON.parse(error._body).toString();
        }

        
      }
    );
  }

  socialLogin(provider: string){
    provider = provider.charAt(0).toUpperCase().concat(provider.substr(1, provider.length-1));
    const loginAction = `loginWith${provider}`;
    this[loginAction]();

  }

  loginWithFacebook() {
      this.loading = true;
      const fbLogin = this.fb.login(this.options);
      fbLogin.then(response => {

        // Fetch user info from Facebook
        const userResult = this.fb.api('/me?fields=email,first_name,last_name');
        userResult.then((fbUser) => {
          console.log(response.authResponse);
          const params = {
            'email': fbUser.email, 
            'mac_address': this.model.mac_address,
            'provider_id': fbUser.id,
            'provider_name': 'facebook',
            'first_name': fbUser.first_name,
            'last_name': fbUser.last_name,
            'access_token': response.authResponse.accessToken
          };

          // Login facebook user with captive portal API
          this.authService.socialLogin(params, this.queryStringParams).subscribe(
            data => {
              this.loading = false;
              console.log('social logged in', data);
              this.success = data.success;
            },
            error => {
                this.loading = false;
                if (error.status === 400){
                  this.error = error._body;
                }
                else{
                  this.error = error.toString();
                }
            }
          );
        });
      });

      fbLogin.catch(error => {
        this.loading = false;
        this.error = error.toString();
      });
  }

  loginWithTwitter() {
    this.loading = true;

    // Login user wiht Twitter
    const twitterLogin = this.authService.loginWithTwitter()
    
    twitterLogin.then(response => {

        // Get user info from Twitter response
        const twitterUser = response.additionalUserInfo.profile
        console.log(response);
        const params = {
          'email': twitterUser.email, 
          'mac_address': this.model.mac_address,
          'provider_id': twitterUser.id,
          'provider_name': 'twitter',
          'access_token': response.credential.accessToken
        };
        
        // Login twitter user with captive portal API
        this.authService.socialLogin(params, this.queryStringParams).subscribe(
          data => {
            this.loading = false;
            console.log('social logged in', data);
            this.success = data.success;
            this.zone.run(() => {});
          },
          error => {
              this.loading = false;
              if (error.status === 400){
                this.error = error._body;
              }
              else{
                this.error = error.toString();
              }
              this.zone.run(() => {});
          }
        );
        
    });

    twitterLogin.catch(error => {
      this.loading = false;
      this.error = error.toString();
      this.zone.run(() => {});
    });
  }

  loginWithGoogle() {
    this.loading = true;

    // Login user with Google
    const googleLogin = this.authService.loginWithGoogle();

    googleLogin.then(response => {
      const googleUser = response.user;
      const params = {
        'email': googleUser.email, 
        'mac_address': this.model.mac_address,
        'provider_id': googleUser.uid,
        'provider_name': 'google',
        'access_token': response.credential.accessToken
      };

      // Login Google user with Captive portal API
      this.authService.socialLogin(params, this.queryStringParams).subscribe(
        data => {
          this.loading = false;
          console.log('social logged in', data);
          this.success = data.success;
          this.zone.run(() => {});
        },
        error => {
            this.loading = false;
            if (error.status === 400){
              this.error = error._body;
            }
            else{
              this.error = error.toString();
            }
            this.zone.run(() => {});
        }
      );      
     
    });

    googleLogin.catch(error => {
      this.loading = false;
      this.error = error.toString();
      this.zone.run(() => {});
    });
  }
}
