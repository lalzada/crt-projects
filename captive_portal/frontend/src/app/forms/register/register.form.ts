import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserModel } from '../../models/user';
import { AuthService } from './../../services/auth.service';

@Component({
  selector: 'register-form',
  templateUrl: './register.form.html',
  styleUrls: ['./register.form.scss'],
})
export class RegisterForm {

  model = new UserModel();
  loading: boolean = false;
  success: string = '';
  error: string = '';
  formErrors: any = {};
  terms: string = '';
  confirm_password: string = '';

  constructor(
    private authService: AuthService,
    private route: ActivatedRoute,
  ){ }

  ngOnInit() {
    
        // http://www.tp-link.com/us/faq-916.html
    
        this.route.queryParams.subscribe(params => {
          this.model.mac_address = params['cid'];
          console.log(params);
        });
      }

  onSubmit = () => {  
    this.loading = true;
    this.authService.register(this.model).subscribe(
      data => {
        this.error = '';
        this.formErrors = {};
        this.loading = false;
        this.success = `Registered successfully. ${JSON.stringify(data)}`;
      },
      error => {
        this.loading = false;
        this.success = '';
        this.formErrors = {};
        if (error.status === 400){
            this.formErrors = JSON.parse(error._body);
            
            if (Object.keys(this.formErrors).length === 1 && this.formErrors.mac_address){
              this.error = JSON.stringify(this.formErrors);
            }
        }
        else{
          this.error = error._body;
        }
      }
    );
  }
}
