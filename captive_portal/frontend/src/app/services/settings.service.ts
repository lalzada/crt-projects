import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { environment as env } from './../../environments/environment';

@Injectable()
export class SettingsService {
  constructor (
    private http: Http,
  ) {
  }

  allowedSocialLogins = () => {
    return this.http
    .get(`${env.api_url}/user/allowed_social_login/list`)
    .map((res:Response) => res.json());
  }

}