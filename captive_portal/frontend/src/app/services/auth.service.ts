import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { environment as env } from './../../environments/environment';

@Injectable()
export class AuthService {
  constructor (
    private http: Http,
    private firebaseAuth: AngularFireAuth
  ) {
  }

  login = (data, queryStringData) => {
    return this.http
    .post(`${env.api_url}/user/login?${queryStringData}`, data)
    .map((res:Response) => res.json());
  }

  register = (data) => {
    return this.http
    .post(`${env.api_url}/user/register`, data)
    .map((res:Response) => res.json());
  }

  loginWithTwitter() {
    return this.firebaseAuth.auth.signInWithPopup(
      new firebase.auth.TwitterAuthProvider()
    )
  }

  loginWithGoogle() {
    return this.firebaseAuth.auth.signInWithPopup(
      new firebase.auth.GoogleAuthProvider()
    )
  }

  socialLogin = (data, queryStringData) => {
    return this.http
    .post(`${env.api_url}/user/login/social?${queryStringData}`, data)
    .map((res:Response) => res.json());
  }

}