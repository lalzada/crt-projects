# Orient Fitting Mirror

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.5.5.

## External APIs
#### Facebook Login:
https://github.com/zyra/ngx-facebook

#### Twitter and Google Login:
https://github.com/angular/angularfire2/blob/master/docs/auth/getting-started.md

Note: If you don't want to use Firebase for Google login, can be replaced by https://github.com/rubenCodeforges/ng-gapi

#### Share video on Facebook:
https://developers.facebook.com/docs/graph-api/video-uploads

## Developers Console we have used: 
Facebook: https://developers.facebook.com/

Google: https://console.developers.google.com/apis/

Twitter: https://apps.twitter.com/

Firebase: https://console.firebase.google.com/u/0/

## Storage
#### LocalStorage for User's session and Videos:
https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
