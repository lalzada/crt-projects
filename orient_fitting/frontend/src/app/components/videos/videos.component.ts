import { Component } from '@angular/core';
import { ProgressHttp } from "angular-progress-http";
import { RequestOptions, Headers } from '@angular/http';
import { FacebookService, InitParams, LoginOptions } from 'ngx-facebook';
import { GoogleApiService, GoogleAuthService } from 'ng-gapi';
import { AuthService } from './../../services/auth.service';


declare var $ :any;

@Component({
  selector: 'videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.scss']
})
export class VideosComponent {

  videos: Array<object> = [];
  uploading: boolean = false;
  uploadProgress: number = 0;
  error: string = '';
  success: string = '';
  options: LoginOptions = {
    scope: 'public_profile,email,publish_actions',
    return_scopes: true
  };

  constructor(
    private http: ProgressHttp,
    private authService: AuthService,
    private fb: FacebookService,
    private gapiService: GoogleApiService,
    private googleAuth: GoogleAuthService
  ) { 
    this.videos = this.authService.getUserVideos();
    console.log(this.videos);
    const initParams: InitParams = {
      appId: '550935518574779',
      xfbml: true,
      version: 'v2.8',
      status: false,
    };

    fb.init(initParams);
  }

  ngAfterViewInit(){
    $('#gallery').gallery();
  }

  toggleVideoPlay = video => {
    // play video on click if puased. pause video on click if playing already.

    // play/pause only that video which is in center of the slide show
    const isVideoCentered = $(video).parent().attr('class') === 'dg-center';
    if (isVideoCentered){
      video.paused ? video.play() : video.pause();
    }
  }

  base64ToFile = (url, filename, mimeType) => {
    // convert base64 string of video to File Object
    return (fetch(url)
        .then(function(res){return res.arrayBuffer();})
        .then(function(buf){return new File([buf], filename, {type:mimeType});})
    );
  } 

  shareOnFacebook = videoWrapper => {

    const videoSource = $(videoWrapper).find('a.dg-center video source').attr('src');
    const result = this.fb.login(this.options);

    result.then(response => {

      this.uploading = true;
      const base64ToFile = this.base64ToFile(videoSource, 'video.mp4', 'video/mp4');

      base64ToFile.then(file => {

        const fbUserId = response.authResponse.userID;
        const accessToken = response.authResponse.accessToken;
        const form = new FormData();
        form.append('source', file);
        form.append('name', 'video.mp4');
        form.append('description', 'Orient Fitting Mirror');


        this.http
        .withUploadProgressListener(progress => this.uploadProgress = progress.percentage)
        .post(`https://graph.facebook.com/me/videos?access_token=${accessToken}`, form)
        .subscribe(response => {
          this.uploading = false;
          this.success = (response.status === 200 && response.ok === true) ? 'Video posted successfully.' :  null;
          this.error = (response.status !== 200 && response.ok !== true) ? 'Video did not post successfully.' :  null;
          if (this.success){
            // response = JSON.parse(response._body);
            // const videoUrl = `https://www.facebook.com/${fbUserId}/videos/${response.id}/`;  
            // this.success += `<br/><a href="${videoUrl}" target="_new">Watch here</a>`;
          }
          
        });

      })
        
    })
    .catch(err => { 
      this.error = err.toString(); 
      this.uploading = false; 
    });
  }

  shareOnGoogle = videoWrapper => {

    // Google doesn't allow to post on Google+ stream.
    // https://stackoverflow.com/questions/7570327/how-to-post-in-google-wall#answer-7570416

    const videoSource = $(videoWrapper).find('a.dg-center video source').attr('src');
    
    this.googleAuth.getAuth().subscribe(auth => {
        auth.signIn().then(res => {

          console.log(res.getAuthResponse().access_token);
          console.log(res.getId());

          const accessToken = res.getAuthResponse().access_token;
          const userId = res.getId();
          const base64ToFile = this.base64ToFile(videoSource, 'video.mp4', 'video/mp4');
          base64ToFile.then(file => {
            let form = new FormData();
            form.append('media_body', videoSource);
            const data = {
              "object": {
                "originalContent": "Happy Monday! #caseofthemondays",
              },
              "access": {
                "items": [{
                    "type": "domain"
                }],
                "domainRestricted": true
              }
            };
            
            let headers = new Headers();
            headers.append('Authorization', `OAuth ${accessToken}`);
            headers.append('Content-Type', `application/json`);

            let options = new RequestOptions({ headers: headers});
    
            this.http
            .withUploadProgressListener(progress => console.log(progress))
            .post(
              `https://www.googleapis.com/plusDomains/v1/people/me/activities`, 
              JSON.stringify(data), options
            )
            .subscribe(response => {
              console.log(response);
            });
    
          });
        });
    });
  }

  shareOnTwitter = videoWrapper => {
    const videoSource = $(videoWrapper).find('a.dg-center video source').attr('src');
    
    this.authService.loginWithTwitter().then(res=>{


        // twitter doesn't allow write requests from client side.
        // we should tweet from server side instead.
        // just pass this video source to server using a new api i.e
        // /api/tweet/ and make request from server to tweet this video
        // get response from server and show on client side.

        // let headers = new Headers();
        // headers.append('Content-Type', `multipart/form-data`);
        // let options = new RequestOptions({ headers: headers});

        // this.http
        // .withUploadProgressListener(progress => console.log(progress))
        // .post(
        //   `https://upload.twitter.com/1.1/media/upload.json`, 
        //   {'media_data': videoSource},
        //   options
        // )
        // .subscribe(response => {
        //   console.log(response);
        // });
   });

  }

}
