import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { AuthService } from './../../services/auth.service';

@Component({
  selector: 'auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  authType: String = ''; // current active route. login or register

  constructor(
    private location: Location,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
  ) { 
    if (this.authService.isAuthenticated()){
      this.router.navigate(['videos']);
    }
   }

  ngOnInit() {
    this.route.params.forEach(params => this.authType = params['authType']);
  }

  goToLogin = () => {
    // this will change the URL without reloading component.
    const url = this.router.createUrlTree(['', 'login'], {relativeTo: this.route}).toString();
    this.location.go(url);
    this.authType = 'login';
  }
 
  goToRegister = () => {
    const url = this.router.createUrlTree(['', 'register'], {relativeTo: this.route}).toString();
    this.location.go(url);
    this.authType = 'register';
  }

}
