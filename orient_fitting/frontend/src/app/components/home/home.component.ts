import { Component } from '@angular/core';
import {Router} from '@angular/router';
import { AuthService } from './../../services/auth.service';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent{

  constructor (
    private authService: AuthService,
    private router: Router,
  ){ 
    if (this.authService.isAuthenticated()){
      this.router.navigate(['videos']);
    }
  }
}
