import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Router } from "@angular/router";
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';
import { environment as env } from './../../environments/environment';

@Injectable()
export class AuthService {
  private user: Observable<firebase.User>;

  constructor (
    private http: Http,
    private _firebaseAuth: AngularFireAuth, 
    private router: Router
  ) {
    this.user = _firebaseAuth.authState;
  }

  login = (data) => {
    return this.http
    .post(`${env.api_url}/${env.api_version}/user/login`, data)
    .map((res:Response) => res.json());
  }

  socialLogin = (data) => {
    return this.http
    .post(`${env.api_url}/${env.api_version}/user/login/social`, data)
    .map((res:Response) => res.json());
  }

  register = (data) => {
    return this.http
    .post(`${env.api_url}/${env.api_version}/user/register`, data)
    .map((res:Response) => res.json());
  }

  loginWithFacebook() {
    return this._firebaseAuth.auth.signInWithPopup(
      new firebase.auth.FacebookAuthProvider()
    )
  }

  loginWithTwitter() {
    return this._firebaseAuth.auth.signInWithPopup(
      new firebase.auth.TwitterAuthProvider()
    )
  }

  loginWithGoogle() {
    return this._firebaseAuth.auth.signInWithPopup(
      new firebase.auth.GoogleAuthProvider()
    )
  }

  isAuthenticated = () => {
    const userId = localStorage.getItem('userId');
    return userId !== null ? true : false;
  }

  canActivate(): boolean{
    if(!this.isAuthenticated()){
      this.router.navigate(['login']);
    }
    return true;
  }

  getUserVideos = () => {
    const videos = localStorage.getItem('userVideos');
    return videos !== null ? JSON.parse(videos) : [];
  }

  saveUserVideos = (videos) => {
    localStorage.setItem('userVideos', JSON.stringify(videos));
  }

  saveSession = (userId, videos) => {
    localStorage.setItem('userId', userId);
    this.saveUserVideos(videos);
  }

  deleteSession = () => {
    localStorage.removeItem('userId');
    localStorage.removeItem('userVideos');
  }

  getUserId = () => {
    return localStorage.getItem('userId');
  }

}