import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthComponent } from './../components/auth/auth.component';
import { HomeComponent } from '../components/home/home.component';
import { VideosComponent } from '../components/videos/videos.component';
import { AuthService } from './../services/auth.service';

const routes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full' },
  { path: 'videos', component: VideosComponent, pathMatch: 'full',
    canActivate: [ AuthService ],
  },
  { path: ':authType', component: AuthComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class RoutingModule { }

