import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserModel } from '../../models/user';
import { AuthService } from './../../services/auth.service';

@Component({
  selector: 'register-form',
  templateUrl: './register.form.html',
  styleUrls: ['./register.form.scss'],
})
export class RegisterForm {

  model = new UserModel();
  loading: boolean = false;
  success: string = '';
  error: string = '';
  confirm_password: string = '';
  terms: string = '';
  formErrors: any = {};

  constructor(
    private authService: AuthService,
    private route: ActivatedRoute,
  ){ }

  ngOnInit() {
    this.route.queryParams.subscribe(params => this.model.session=params['session']);
  }

  onSubmit = () => {  
    this.loading = true;
    console.log(this.model);
    this.authService.register(this.model).subscribe(
      data => {
        this.formErrors = {};
        console.log('registered', data.id);
        this.loading = false;
        this.success = 'Registered successfully. Please login.';
      },
      error => {
        console.log(error);
        if (error.status === 403){
            this.error = error.json().detail;
        }
        else if (error.status === 400){
            this.formErrors = error.json();
        }
        else {
          this.error = error.toString();
        }

        this.loading = false;
      }
    );
  }
}
