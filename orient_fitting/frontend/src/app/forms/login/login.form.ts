import { Component, OnInit , NgZone} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FacebookService, LoginResponse, InitParams, LoginOptions } from 'ngx-facebook';

import { UserModel } from '../../models/user';
import { AuthService } from './../../services/auth.service';


@Component({
  selector: 'login-form',
  templateUrl: './login.form.html',
  styleUrls: ['./login.form.scss'],
})
export class LoginForm {

  model = new UserModel();
  loading: boolean = false;
  error: string = '';
  formErrors: any = {};
  confirm_password: string = '';
  terms: string = '';
  options: LoginOptions = {
    scope: 'public_profile,email',
    return_scopes: true
  };

  constructor(
    private authService: AuthService,
    private fb: FacebookService,
    private router: Router,
    private route: ActivatedRoute,
    private zone: NgZone
  ){
    const initParams: InitParams = {
      appId: '550935518574779',
      xfbml: true,
      version: 'v2.8',
      status: false,
    };

    fb.init(initParams);
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => this.model.session=params['session']);
  }

  login = () => {  
    this.loading = true;
    this.authService.login(this.model).subscribe(
      data => {
        console.log('logged in', data.id);
        const videos = [];
        data.sessions.forEach(session => {
          session.videos.forEach(video => videos.push(video));
        });
        this.authService.saveSession(data.id, videos);
        this.router.navigate(['/videos']);
      },
      error => {
        this.error = error.json();
        if (error.status === 403){
            this.error = error.json().detail;
        }
        else if (error.status === 400){
            this.formErrors = error.json();
            if ('session' in this.formErrors){
              this.error = this.formErrors['session'];
              this.error = `session: ${this.error}`;
            }
        }
        else {
          this.error = error.toString();
        }

        this.loading = false;
      }
    )
  }

  loginWithFacebook() {
      this.loading = true;
      const result = this.fb.login(this.options);
      result.then(response => {
        const userResult = this.fb.api('/me?fields=email,first_name,last_name');
        userResult.then((fbUser) => {
          this.model.email = fbUser.email;
          this.model.first_name = fbUser.first_name;
          this.model.last_name = fbUser.last_name;
          this.model.provider_id = fbUser.id;
          this.model.provider_name = 'facebook';
          this.authService.socialLogin(this.model).subscribe(
            data => {
                this.loading = false;
                const videos = [];
                data.sessions.forEach(session => {
                  session.videos.forEach(video => videos.push(video));
                });
                this.authService.saveSession(data.id, videos);
                console.log('logged in. redirect to videos');
                this.router.navigate(['/videos']);
            },
          error => {
              this.loading = false;
              if (error.status === 400){
                  this.formErrors = error.json();
                  if ('session' in this.formErrors){
                    this.error = this.formErrors['session'];
                    this.error = `session: ${this.error}`;
                  }
              }
              else{
                this.error = error.toString();
              }
          }

          );
        });
      })

      result.catch(error => {
        this.loading = false;
        this.error = error.toString();
      });
  }

  loginWithTwitter = () => {
    this.loading = true;
    const result = this.authService.loginWithTwitter()
    result.then(response => {
        const twitterUser = response.user;
        this.model.email = twitterUser.email;
        this.model.provider_id = twitterUser.uid;
        this.model.provider_name = 'twitter';
        this.authService.socialLogin(this.model).subscribe(
          data => {
              this.loading = false;
              console.log('login', data.id);
              const videos = [];
              data.sessions.forEach(session => {
                session.videos.forEach(video => videos.push(video));
              });
              this.authService.saveSession(data.id, videos);
              this.router.navigate(['/videos']);
          },
          error => {
              this.loading = false;
              if (error.status === 400){
                  this.formErrors = error.json();
                  if ('session' in this.formErrors){
                    this.error = this.formErrors['session'];
                    this.error = `session: ${this.error}`;
                  }
              }
              else{
                this.error = error.toString();
              }
            this.zone.run(() => {});
          }
        );    
    });

    result.catch(error => {
      this.loading = false;
      this.error = error.toString();
      this.zone.run(() => {});
    });
  }

  loginWithGoogle = () => {
    this.loading = true;
    const result = this.authService.loginWithGoogle();
    result.then(response => {
      const googleUser = response.user;
      this.model.email = googleUser.email;
      this.model.provider_name = 'google';
      this.model.email = googleUser.email;
      this.model.provider_id = googleUser.uid;
      this.authService.socialLogin(this.model).subscribe(
        data => {
            console.log('login...', this.model);
            this.loading = false;
            const videos = [];
            data.sessions.forEach(session => {
              session.videos.forEach(video => videos.push(video));
            });
            this.authService.saveSession(data.id, videos);
            console.log('logged in. redirect to videos.....');
            this.router.navigate(['/videos']);
        },
          error => {
              this.loading = false;
              if (error.status === 400){
                  this.formErrors = error.json();
                  if ('session' in this.formErrors){
                    this.error = this.formErrors['session'];
                    this.error = `session: ${this.error}`;
                  }
              }
              else{
                this.error = error.toString();
              }
            this.zone.run(() => {});
          }
      );
      });

    result.catch(error => {
      this.loading = false;
      this.error = error.toString();
      this.zone.run(() => {});
    });
  }
}
