import { Component, NgZone } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  
  showBackgroundImage: Boolean = false;

  constructor(
    private router:Router,
    private zone: NgZone,
    public authService: AuthService
  ) { }

  ngOnInit() {
    this.router.events.filter(event=>event instanceof NavigationEnd)
    .subscribe(event=>{
      const isAuthUrl = (this.router.url === '/login' || this.router.url === '/register') ? true : false;
      this.showBackgroundImage = (!isAuthUrl && event instanceof NavigationEnd) ? true : false;
      // either this navigationEnd is finishing after component is loaded
      // or state change is not updating component so force component to re-render
      this.zone.run(()=>{}); 
      console.log('nav end');
    });
  }

  logout = () => {
    this.authService.deleteSession();
    this.router.navigate(['']);
  }

}
