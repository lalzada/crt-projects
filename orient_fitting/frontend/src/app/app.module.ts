import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule }   from '@angular/forms';
import { FacebookModule } from 'ngx-facebook';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { ProgressHttpModule } from "angular-progress-http";
import { GoogleApiModule, NG_GAPI_CONFIG} from "ng-gapi";

import { environment } from '../environments/environment';

import { AppComponent } from './app.component';
import { AuthComponent } from './components/auth/auth.component';
import { RoutingModule } from './routing/routing.module';
import { AuthService } from './services/auth.service';
import { LoginForm } from './forms/login/login.form';
import { RegisterForm } from './forms/register/register.form';
import { HomeComponent } from './components/home/home.component';
import { VideosComponent } from './components/videos/videos.component';
import { NgGapiClientConfig } from './interfaces/google.api';

// remove ng-gapi package and its related settings/files if we are not posting to google+ stream.
// we are using firebase package for google login
// or if you want to not use firebase for google login. use ng-gapi and take sample login
// code form videos.components.ts (share on google) function

let gapiClientConfig: NgGapiClientConfig = {
  client_id: "714395160247-8hjs62bt7kr21f7bap4mcjk6jkf9cvuu.apps.googleusercontent.com",
  discoveryDocs: [],
  scope: [
    "https://www.googleapis.com/auth/plus.me",
    "https://www.googleapis.com/auth/plus.profiles.read",
    "https://www.googleapis.com/auth/plus.media.upload",
    "https://www.googleapis.com/auth/plus.stream.write"
  ].join(" ")
};

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    LoginForm,
    RegisterForm,
    HomeComponent,
    VideosComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RoutingModule,
    HttpModule,
    FacebookModule.forRoot(),
    AngularFireModule.initializeApp(environment.firebase, 'angular-auth-firebase'),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    ProgressHttpModule,
    GoogleApiModule.forRoot({
      provide: NG_GAPI_CONFIG,
      useValue: gapiClientConfig
    }),
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
