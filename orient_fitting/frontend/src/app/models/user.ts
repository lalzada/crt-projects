export class UserModel {
    constructor(
        public email?: string,
        public is_mobile_login: boolean=true,
        public session: string = '',
        public phone?: string,
        public password?: string,
        public provider_id?: number,
        public provider_name?: string,
        public first_name?: string,
        public last_name?: string,
      ){}
}
