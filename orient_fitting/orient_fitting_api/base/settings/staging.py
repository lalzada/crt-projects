from .base import *
import mongoengine

SECRET_KEY = 'ns^489a&sb$u(&l8pcutz3a0s@n)4b1c)gs(qf5q0q@6rrb7za'

DEBUG = False

ALLOWED_HOSTS = ['*']

mongoengine.connect(db='orient-fitting-api')

STATIC_ROOT = os.path.join(BASE_DIR, '..', 'static')

STATIC_URL = 'https://cportal.cressettech.net/orient-fitting-api/static/'


# ADMINS will be notified of 500 errors by email.
ADMINS = [
    ('Lal Zada', 'lal.zada@cressettech.co')
]


# MANAGERS will be notified of 404 errors
MANAGERS = [
    ('Lal Zada', 'lal.zada@cressettech.co')
]

if not DEBUG:
    INSTALLED_APPS += ['django.contrib.staticfiles']


CORS_ORIGIN_ALLOW_ALL = True
CORS_ORIGIN_WHITELIST = (
    '172.16.190.25',
    '127.0.0.1:4200',
    'localhost.socialauth.com:4200'
)

