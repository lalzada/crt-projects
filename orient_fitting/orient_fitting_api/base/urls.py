from django.urls import path, include
from rest_framework_swagger.views import get_swagger_view
from django.conf.urls.static import static
from django.conf import settings
import os

urlpatterns = [
    path('v1/', include('v1.urls', namespace='v1'))
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

baseUrl = '/'

if os.environ['DJANGO_SETTINGS_MODULE'] == 'base.settings.staging':
    baseUrl = '/orient-fitting-api/'


schema_view = get_swagger_view(
    title='Orient Fitting API', patterns=urlpatterns, url=baseUrl)

urlpatterns += [path('', schema_view)]
