from rest_framework_mongoengine import generics
from mongoengine.django.mongo_auth.models import get_user_document
from v1.authentication import IsUserLoggedIn
from v1.video import serializers
from v1.video.models import Video, Session

UserModel = get_user_document()


class VideoCreate(generics.CreateAPIView):
    serializer_class = serializers.VideoSerializer
    queryset = Video.objects.all()
    authentication_classes = (IsUserLoggedIn,)

    def perform_create(self, serializer):
        serializer.save(session=serializer.validated_data.get('session').to_dbref())


class VideoList(generics.GenericAPIView):
    serializer_class = serializers.VideoListSerializer
    queryset = Video.objects.all()

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user_sessions = Session.objects.filter(user=serializer.validated_data.get('user_id').to_dbref())
        user_videos = Video.objects.filter(session__in=user_sessions)
        user_videos = self.paginate_queryset(user_videos)
        videos = [video.to_json() for video in user_videos]
        return self.get_paginated_response(videos)







