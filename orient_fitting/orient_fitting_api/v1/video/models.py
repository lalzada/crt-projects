import mongoengine
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible
from mongoengine import fields, Document
import base64


@python_2_unicode_compatible
class Session(Document):
    key = fields.StringField(required=True)
    user = fields.ReferenceField('v1.user.models.User', reverse_delete_rule=mongoengine.CASCADE, required=False)
    created_at = fields.DateTimeField(default=timezone.now)

    def __unicode__(self):
        return self.key

    meta = {
        'indexes': [
            {'fields': ['key'], 'unique': True, 'sparse': True}
        ]
    }

    def to_json(self):
        data = self.to_mongo()
        if '_cls' in data.keys():
            data.pop('_cls')

        data['id'] = str(data.pop('_id'))
        data['user'] = str(data.get('user'))
        data['videos'] = [video.to_json() for video in Video.objects.filter(session__in=[self])]
        return data


@python_2_unicode_compatible
class Video(Document):
    data = fields.BinaryField(required=True)
    mime_type = fields.StringField(required=True)
    duration = fields.IntField()
    is_deleted = fields.BooleanField(default=False)

    session = fields.ReferenceField(Session, reverse_delete_rule=mongoengine.CASCADE)

    def __unicode__(self):
        return 'Data: {} Mime Type: {}'.format(self.data, self.mime_type)

    meta = {
        'indexes': [
            {'fields': ['session']}
        ]
    }

    def save(self, *args, **kwargs):
        self.data = base64.b64decode(self.data)
        return super(Video, self).save(*args, **kwargs)

    def to_json(self):
        data = self.to_mongo()
        if '_cls' in data.keys():
            data.pop('_cls')

        data['id'] = str(data.pop('_id'))
        data['data'] = base64.b64encode(data.pop('data'))
        data['session'] = str(data.get('session'))
        return data
