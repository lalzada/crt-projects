from django.urls import path
from v1.video import views

app_name = 'video'

urlpatterns = [
    path('create', views.VideoCreate.as_view(), name='create'),
    path('list', views.VideoList.as_view(), name='list'),
]
