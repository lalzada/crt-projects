from rest_framework import serializers
from mongoengine.django.mongo_auth.models import get_user_document
from rest_framework.exceptions import NotFound, ValidationError
import bson
import base64
import binascii
from .models import Video, Session

UserModel = get_user_document()


class VideoSerializer(serializers.Serializer):

    session = serializers.CharField()
    data = serializers.CharField()
    mime_type = serializers.CharField()
    duration = serializers.IntegerField()

    class Meta:
        model = Video
        fields = '__all__'

    @staticmethod
    def validate_data(value):
        try:
            base64.b64decode(value)
        except binascii.Error:
            raise ValidationError('Invalid video data. Video data must be base64 encoded string')

        return value

    def validate_session(self, value):
        user = self.context['request'].user
        try:
            session = Session.objects.get(key=value, user=user)
        except Session.DoesNotExist:
            session = Session.objects.create(key=value, user=user.to_dbref())
        return session

    @staticmethod
    def validate_mime_type(value):
        if not value.startswith('video/'):
            raise ValidationError('Mime Type must be video/*')

        return value

    def create(self, validated_data):
        return Video.objects.create(**validated_data)


class VideoListSerializer(serializers.Serializer):

    user_id = serializers.CharField()

    class Meta:
        model = Video
        fields = '__all__'

    @staticmethod
    def validate_user_id(value):
        if not bson.objectid.ObjectId.is_valid(value):
            raise NotFound(detail='User with provided id does not exist.')

        try:
            user = UserModel.objects.get(id=bson.ObjectId(value))
        except UserModel.DoesNotExist:
            raise NotFound(detail='User with provided id does not exist.')
        return user
