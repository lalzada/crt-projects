from django.urls import path
from v1.admin import views

app_name = 'admin'

urlpatterns = [
    path('login', views.AdminLogin.as_view(), name='login'),
]
