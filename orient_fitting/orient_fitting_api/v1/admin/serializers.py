from rest_framework.exceptions import AuthenticationFailed
from rest_framework_mongoengine.serializers import DocumentSerializer
from .models import Admin


class AdminLoginSerializer(DocumentSerializer):

    class Meta:
        model = Admin
        fields = '__all__'

    def validate(self, data):
        super(AdminLoginSerializer, self).validate(data)
        try:
            Admin.objects.get(
                username=data.get('username'),
                password=data.get('password')
            )

        except Admin.DoesNotExist:
            raise AuthenticationFailed(detail='Invalid username or password')
        return data
