from mongoengine import Document
from mongoengine import fields
from datetime import datetime
import binascii
import os


class Admin(Document):
    username = fields.StringField(required=True)
    password = fields.StringField(required=True)

    def __unicode__(self):
        return self.username

    def save(self, *args, **kwargs):
        super(Admin, self).save(*args, **kwargs)
        if not Token.objects.filter(user=self.to_dbref()).count() > 0:
            Token.objects.create(user=self.to_dbref())

        return self

    def to_json(self):
        data = self.to_mongo()

        if '_cls' in data.keys():
            data.pop('_cls')

        data.pop('password')
        data['id'] = str(data.pop('_id'))
        try:
            token = Token.objects.get(user=self.to_dbref()).key
        except Token.DoesNotExist:
            token = Token.objects.create(user=self.to_dbref()).key
        data['token'] = token
        return data


class Token(Document):
    key = fields.StringField(max_length=44)
    user = fields.ReferenceField(Admin, required=True)
    created = fields.DateTimeField()

    def __init__(self, *args, **kwargs):
        super(Token, self).__init__(*args, **kwargs)
        if not self.key:
            self.key = self.generate_key()

    def save(self, *args, **kwargs):
        if not self.id:
            self.created = datetime.now()

        return super(Token, self).save(*args, **kwargs)

    @staticmethod
    def generate_key():
        return binascii.hexlify(os.urandom(22)).decode()

    @staticmethod
    def validate_token(token=None):
        try:
            token_user = Token.objects.get(key=token)
        except Token.DoesNotExist:
            return False

        try:
            user = Admin.objects.get(id=str(token_user.user.id))
            return user
        except Admin.DoesNotExist:
            return False

    def __unicode__(self):
        return self.key

    def to_json(self):
        data = self.to_mongo()

        if '_cls' in data.keys():
            data.pop('_cls')

        data['id'] = str(data.pop('_id'))

        return data


