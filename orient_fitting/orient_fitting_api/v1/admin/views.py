from v1.admin.models import Admin
from rest_framework.response import Response
from rest_framework_mongoengine import generics

from v1.admin import serializers


class AdminLogin(generics.GenericAPIView):
    serializer_class = serializers.AdminLoginSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = Admin.objects.get(username=serializer.validated_data.get('username'))
        user = user.to_json()
        request.session['admin'] = user

        return Response(user)

