"""
This file contains all basic models which are supposed to be inherit by
other models
"""

from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.hashers import check_password, make_password
from django.contrib.auth.models import \
    _user_has_perm, _user_get_all_permissions, _user_has_module_perms

from mongoengine.django import auth
from mongoengine import fields, Document
from v1.video import models as video_models


class AbstractBaseUser(Document):
    """
    Base User model having all related fields and methods required by a User.
    This is a copy of Django Abstract User Model and Abstract Base User Model

    https://github.com/django/django/blob/master/django/contrib/auth/models.py#L288
    https://github.com/django/django/blob/master/django/contrib/auth/base_user.py#L47
    """
    password = fields.StringField(max_length=128)
    first_name = fields.StringField()
    last_name = fields.StringField()

    is_staff = fields.BooleanField(default=False)
    is_superuser = fields.BooleanField(default=False)
    is_active = fields.BooleanField(default=True)

    last_login = fields.DateTimeField(default=timezone.now, verbose_name=_('last login'))
    date_joined = fields.DateTimeField(default=timezone.now, verbose_name=_('date joined'))
    user_permissions = fields.ListField(fields.ReferenceField(auth.Permission))

    REQUIRED_FIELDS = ()

    meta = {
        'allow_inheritance': True,
        'abstract': True,
    }

    def __unicode__(self):
        return '{}{}'.format(self.first_name, self.last_name)

    @property
    def full_name(self):
        return '{}{}'.format(self.first_name, self.last_name)

    def is_anonymous(self):
        return False

    def is_authenticated(self):
        return True

    def set_password(self, raw_password):
        """
        Sets the user's password - always use this rather than directly
        assigning to :attr:`password` as the password is hashed before storage.
        """
        self.password = make_password(raw_password)
        return self

    def check_password(self, raw_password):
        """
        Checks the user's password against a provided password - always use
        this rather than directly comparing to :attr:`password` as the
        password is hashed before storage.
        """
        return check_password(raw_password, self.password)

    def get_group_permissions(self, obj=None):
        """
        Returns a list of permission strings that this user has through his/her
        groups. This method queries all available auth backends. If an object
        is passed in, only permissions matching this object are returned.
        """
        permissions = set()
        for backend in auth.get_backends():
            if hasattr(backend, "get_group_permissions"):
                permissions.update(backend.get_group_permissions(self, obj))
        return permissions

    def get_all_permissions(self, obj=None):
        return _user_get_all_permissions(self, obj)

    def has_perm(self, perm, obj=None):
        """
        Returns True if the user has the specified permission. This method
        queries all available auth backends, but returns immediately if any
        backend returns True. Thus, a user who has permission from a single
        auth backend is assumed to have permission in general. If an object is
        provided, permissions for this specific object are checked.
        """

        # Active superusers have all permissions.
        if self.is_active and self.is_superuser:
            return True

        # Otherwise we need to check the backends.
        return _user_has_perm(self, perm, obj)

    def has_module_perms(self, app_label):
        """
        Returns True if the user has any permissions in the given app label.
        Uses pretty much the same logic as has_perm, above.
        """
        # Active superusers have all permissions.
        if self.is_active and self.is_superuser:
            return True

        return _user_has_module_perms(self, app_label)

    def to_json(self):
        data = self.to_mongo()
        if '_cls' in data.keys():
            data.pop('_cls')

        if 'password' in data.keys():
            data.pop('password')

        data.pop('user_permissions')
        data['id'] = str(data.pop('_id'))
        sessions = video_models.Session.objects.filter(user__in=[self])
        data['sessions'] = [session.to_json() for session in sessions]
        return data





