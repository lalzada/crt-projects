from django.contrib import auth
from mongoengine.django.auth import MongoEngineBackend


class CustomAuthentication(MongoEngineBackend):
    """
    Authenticate using MongoEngine and settings.MONGO_USER_DOCUMENT
    Authenticate user against phone and password
    """

    def authenticate(self, request=None, phone=None, password=None):
        print(phone, password, 'custom auth')
        try:
            user = self.user_document.objects.get(phone=phone)
        except self.user_document.DoesNotExist:
            return None

        if user:
            if password and user.check_password(password):
                backend = auth.get_backends()[0]
                user.backend = "%s.%s" % (backend.__module__, backend.__class__.__name__)
                return user
        return None


class SocialAuthentication(MongoEngineBackend):
    """
    Authenticate using MongoEngine and settings.MONGO_USER_DOCUMENT
    Authenticate user against email and provider_name
    """

    def authenticate(self, request=None, email=None, provider_name=None):
        try:
            user = self.user_document.objects.get(
                social_profile__email=email,
                social_profile__provider_name=provider_name
            )
        except self.user_document.DoesNotExist:
            return None

        return user


