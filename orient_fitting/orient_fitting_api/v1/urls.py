from django.urls import path, include

app_name = 'v1'

urlpatterns = [
    path('user/', include('v1.user.urls', namespace='user')),
    path('video/', include('v1.video.urls', namespace='video')),
    path('image/', include('v1.image.urls', namespace='image')),
    path('admin/', include('v1.admin.urls', namespace='admin'))
]
