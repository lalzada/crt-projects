from rest_framework_mongoengine import generics
from rest_framework.views import View
from rest_framework.response import Response
from mongoengine.django.mongo_auth.models import get_user_document
from rest_framework import status
from v1.user import serializers

UserModel = get_user_document()


class CustomLogin(generics.GenericAPIView):
    serializer_class = serializers.CustomLoginSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        UserModel.logout_all()
        current_user = UserModel.objects.get(phone=serializer.validated_data.get('phone'))
        current_user.login()
        return Response(current_user.to_json(), status=status.HTTP_200_OK)


class CustomRegister(generics.CreateAPIView):
    serializer_class = serializers.CustomRegisterSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        UserModel.logout_all()
        current_user = UserModel.objects.get(phone=serializer.validated_data.get('phone'))
        current_user.login()
        return Response(current_user.to_json(), status=status.HTTP_200_OK)

    def perform_create(self, serializer):
        return serializer.save()


class SocialLogin(generics.GenericAPIView):
    serializer_class = serializers.SocialLoginSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        UserModel.logout_all()
        data = serializer.validated_data
        current_user = UserModel.objects.get(
            social_profile__email=data.get('email'),
            social_profile__provider_name=data.get('provider_name'))
        current_user.login()
        return Response(current_user.to_json(), status=status.HTTP_200_OK)


class UserList(generics.ListAPIView):
    serializer_class = serializers.UserListSerializer
    queryset = UserModel.objects.all()

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        queryset = self.paginate_queryset(queryset)
        users = [user.to_json() for user in queryset]
        return self.get_paginated_response(users)


class CheckLoggedInUser(generics.GenericAPIView):
    http_method_names = ['get']

    @staticmethod
    def get(request):
        users = UserModel.objects.filter(is_logged_in=True)
        if users.count() > 0:
            return Response({"user": str(users.first().id)})

        return Response({"user": False})





