import mongoengine
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible
from mongoengine import fields, EmbeddedDocument

from v1 import models as base_models


@python_2_unicode_compatible
class SocialProfile(EmbeddedDocument):

    PROVIDER_CHOICES = (
        ('facebook', 'Facebook'),
        ('google', 'Google'),
        ('twitter', 'Twitter')
    )

    provider_id = fields.StringField(required=True)
    provider_name = fields.StringField(choices=PROVIDER_CHOICES, required=True)
    email = fields.EmailField(required=True)

    def __unicode__(self):
        return self.email


@python_2_unicode_compatible
class User(base_models.AbstractBaseUser):
    phone = fields.StringField()
    is_logged_in = fields.BooleanField(default=False)
    created_at = fields.DateTimeField(default=timezone.now)

    social_profile = fields.EmbeddedDocumentField(SocialProfile)

    USERNAME_FIELD = 'phone'

    meta = {
        'indexes': [
            {'fields': ['phone']}
        ]
    }

    def __unicode__(self):
        return self.phone if self.phone else self.social_profile.email

    def login(self):
        self.is_logged_in = True
        self.save()

    @classmethod
    def logout_all(cls):
        cls.objects.update(is_logged_in=False)

