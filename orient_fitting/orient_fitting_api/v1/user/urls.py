from django.urls import path
from v1.user import views

app_name = 'user'

urlpatterns = [
    path('login', views.CustomLogin.as_view(), name='login'),
    path('register', views.CustomRegister.as_view(), name='register'),
    path('login/social', views.SocialLogin.as_view(), name='social-login'),
    path('list', views.UserList.as_view(), name='list'),
    path('is_logged_in', views.CheckLoggedInUser.as_view(), name='is-logged-in')
]
