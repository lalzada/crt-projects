from mongoengine.django.mongo_auth.models import get_user_document
from rest_framework.exceptions import AuthenticationFailed, ValidationError
from rest_framework_mongoengine.serializers import DocumentSerializer, EmbeddedDocumentSerializer
from django.contrib.auth.hashers import make_password
from rest_framework import serializers
from django.contrib.auth import authenticate
from .models import SocialProfile
from v1.video.models import Session

UserModel = get_user_document()


class CustomLoginSerializer(DocumentSerializer):

    session = serializers.CharField(required=True)

    class Meta:
        model = UserModel
        fields = ('phone', 'password', 'session')

    def validate(self, data):
        super(CustomLoginSerializer, self).validate(data)
        user = authenticate(
            self.context['request'],
            phone=data.get('phone'),
            password=data.get('password')
        )
        if user is None:
            raise AuthenticationFailed()

        # create new session for current user
        try:
            Session.objects.get(key=data.get('session'))
        except Session.DoesNotExist:
            Session.objects.create(user=user.to_dbref(), key=data.get('session'))
        return data


class CustomRegisterSerializer(DocumentSerializer):

    class Meta:
        model = UserModel
        fields = ('phone', 'password', 'first_name', 'last_name',)

    @staticmethod
    def validate_phone(value):
        try:
            UserModel.objects.get(phone=value)
            raise ValidationError(
                detail='Someone is already registered using provided phone number')
        except UserModel.DoesNotExist:
            pass
        return value

    def create(self, validated_data):
        # just hashing password here and leave rest of things to parent' create
        password = validated_data.pop('password')
        validated_data['password'] = make_password(password)
        return super(CustomRegisterSerializer, self).create(validated_data)


class SocialLoginSerializer(serializers.Serializer):

    first_name = serializers.CharField(required=False)
    last_name = serializers.CharField(required=False)
    provider_id = serializers.CharField()
    provider_name = serializers.ChoiceField(choices=SocialProfile.PROVIDER_CHOICES)
    email = serializers.EmailField()
    session = serializers.CharField(required=True)

    class Meta:
        model = UserModel
        fields = '__all__'

    def validate(self, data):
        data = super(SocialLoginSerializer, self).validate(data)
        user = authenticate(
                    self.context['request'],
                    email=data.get('email'),
                    provider_name=data.get('provider_name')
                )

        """
        in case user is trying to login with any social site i.e Facebook, Google
        Twitter, we are first checking if user already exist with given email and 
        provider_name (twitter, facebook etc). If yes then do nothing. 
        If user doesn't exist, create a new social user
        """
        if user is None:
            self.create_user(data)

        return data

    @staticmethod
    def create_user(data):
        social_profile = SocialProfile(
            email=data.get('email'),
            provider_name=data.get('provider_name'),
            provider_id=data.get('provider_id')
        )
        user = UserModel(first_name=data.get('first_name'), last_name=data.get('last_name'))
        user.social_profile = social_profile
        user.save()

        # create new session for current user
        try:
            Session.objects.get(key=data.get('session'))
        except Session.DoesNotExist:
            Session.objects.create(user=user.to_dbref(), key=data.get('session'))
        return user


class UserListSerializer(DocumentSerializer):
    class Meta:
        model = UserModel
        fields = '__all__'

