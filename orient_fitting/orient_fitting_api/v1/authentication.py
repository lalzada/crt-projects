from mongoengine.django.mongo_auth.models import get_user_document
from rest_framework import exceptions
from rest_framework.authentication import BaseAuthentication, TokenAuthentication
from rest_framework.authentication import get_authorization_header
from rest_framework.exceptions import AuthenticationFailed

from v1.admin.models import Token


class IsUserLoggedIn(BaseAuthentication):
    def authenticate(self, request):
        logged_in_users = get_user_document().objects.filter(is_logged_in=True)
        if logged_in_users.count() > 0:
            user = logged_in_users.first()
            return user, None

        raise AuthenticationFailed('A user must be logged in to create video')


class IsTokenPassed(TokenAuthentication):
    """
    Simple token based authentication.

    Clients should authenticate by passing the token key in the "Authorization"
    HTTP header, prepended with the string "Token ".  For example:

    Authorization: Token 401f7ac837da42b97f613d789819ff93537bee6a
    """

    keyword = 'Token'
    model = Token

    def authenticate(self, request):
        auth = get_authorization_header(request).split()

        # This condition was supposed to raise error in case of missing Authorization header
        # if it didn't so we override it here and leave rest of the things to super class
        if not auth or auth[0].lower() != self.keyword.lower().encode():
            raise exceptions.AuthenticationFailed(detail='Authentication credentials were not provided')

        return super(IsTokenPassed, self).authenticate(request)

    def authenticate_credentials(self, key):
        """
        Overriding just to replace sql Token model with Mongo Token model
        """
        try:
            token = Token.objects.get(key=key)
        except Token.DoesNotExist:
            raise AuthenticationFailed('Invalid token.')

        return token.user, token



