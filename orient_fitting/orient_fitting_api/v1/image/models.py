from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible
from mongoengine import fields, Document
import base64


@python_2_unicode_compatible
class Image(Document):

    TYPE_CHOICES = (
        ('splash', 'Splash Screen'),
        ('logo', 'Logo'),
        ('scroll_category', 'Scroll Category')
    )

    type = fields.StringField(required=True, choices=TYPE_CHOICES)
    data = fields.BinaryField(required=True)
    mime_type = fields.StringField(required=True)
    display_order = fields.IntField(required=True)
    is_deleted = fields.BooleanField(default=False)
    created_at = fields.DateTimeField(default=timezone.now)

    def __unicode__(self):
        return '{}{}'.format(self.data, self.type)

    def save(self, *args, **kwargs):
        self.data = base64.b64decode(self.data)
        return super(Image, self).save(*args, **kwargs)

    def to_json(self):
        data = self.to_mongo()
        if '_cls' in data.keys():
            data.pop('_cls')

        data['id'] = str(data.pop('_id'))
        data['data'] = base64.b64encode(data.get('data'))
        return data
