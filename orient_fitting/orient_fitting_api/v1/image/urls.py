from django.urls import path
from v1.image import views

app_name = 'image'

urlpatterns = [
    path('create', views.ImageCreate.as_view(), name='create'),
    path('list', views.ImageList.as_view(), name='list'),
    path('delete', views.ImageDelete.as_view(), name='delete'),
    path('edit', views.ImageEdit.as_view(), name='edit'),
    path('detail', views.ImageDetail.as_view(), name='detail')
]
