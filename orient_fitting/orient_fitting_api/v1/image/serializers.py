from rest_framework_mongoengine.serializers import DocumentSerializer
from rest_framework import serializers
from rest_framework.exceptions import NotFound, ValidationError
import bson
import base64
import binascii
from .models import Image


class ImageCreateSerializer(DocumentSerializer):

    class Meta:
        model = Image
        fields = '__all__'
        read_only_fields = ('is_deleted', 'created_at')

    @staticmethod
    def validate_data(value):
        try:
            base64.b64decode(value)
        except binascii.Error:
            raise ValidationError('Invalid image data. Image data must be base64 encoded string')

        return value

    @staticmethod
    def validate_mime_type(value):
        if not value.startswith('image/'):
            raise ValidationError('Mime Type must be image/*')

        return value


class ImageDeleteSerializer(serializers.Serializer):
    id = serializers.CharField()

    class Meta:
        model = Image
        fields = ('id',)

    def validate(self, data):
        super(ImageDeleteSerializer, self).validate(data)
        if not bson.objectid.ObjectId.is_valid(data.get('id')):
            raise NotFound(detail='Image not found')

        try:
            image = Image.objects.get(id=bson.ObjectId(data.get('id')))
            image.update(is_deleted=True)
        except Image.DoesNotExist:
            raise NotFound(detail='Image not found')

        return data


class ImageListSerializer(serializers.Serializer):

    image_type = serializers.ChoiceField(choices=Image.TYPE_CHOICES)

    class Meta:
        model = Image
        fields = '__all__'

    @staticmethod
    def validate_image_type(value):
        if value not in list(dict(Image.TYPE_CHOICES).keys()):
            raise NotFound(detail='Invalid image type. Must be one of the following'
                                  ' {}'.format(", ".join(Image.TYPE_CHOICES)))

        return value


class ImageEditSerializer(serializers.Serializer):
    id = serializers.CharField()
    data = serializers.CharField(required=False)
    mime_type = serializers.CharField(required=False)
    display_order = serializers.IntegerField(required=False)
    is_deleted = serializers.BooleanField(required=False)

    class Meta:
        model = Image
        fields = '__all__'

    @staticmethod
    def validate_data(value):
        try:
            base64.b64decode(value)
        except binascii.Error:
            raise ValidationError('Invalid image data. Image data must be base64 encoded string')

        return value

    @staticmethod
    def validate_mime_type(value):
        if not value.startswith('image/'):
            raise ValidationError('Mime Type must be image/*')

        return value

    def validate(self, data):
        super(ImageEditSerializer, self).validate(data)
        if not bson.objectid.ObjectId.is_valid(data.get('id')):
            raise NotFound(detail='Image not found')

        try:
            image = Image.objects.get(id=bson.ObjectId(data.get('id')))
            image.data = data.get('data', image.data)
            image.mime_type = data.get('mime_type', image.mime_type)
            image.display_order = data.get('display_order', image.display_order)
            image.is_deleted = data.get('is_deleted', image.is_deleted)
            image.save()
        except Image.DoesNotExist:
            raise NotFound(detail='Image not found')

        return data


class ImageDetailSerializer(serializers.Serializer):
    id = serializers.CharField()

    class Meta:
        fields = '__all__'

    def validate(self, data):
        super(ImageDetailSerializer, self).validate(data)
        if not bson.objectid.ObjectId.is_valid(data.get('id')):
            raise NotFound(detail='Image not found')

        try:
            image = Image.objects.get(id=bson.ObjectId(data.get('id')))
        except Image.DoesNotExist:
            raise NotFound(detail='Image not found')

        return data

