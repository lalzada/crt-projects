from rest_framework_mongoengine import generics
from rest_framework.response import Response
from bson import ObjectId
from rest_framework import status
from v1.image import serializers, models
from v1.authentication import IsTokenPassed
from v1.permissions import IsAdmin


class ImageCreate(generics.CreateAPIView):
    serializer_class = serializers.ImageCreateSerializer
    queryset = models.Image.objects.all()
    authentication_classes = (IsTokenPassed, )
    permission_classes = (IsAdmin,)


class ImageList(generics.GenericAPIView):
    serializer_class = serializers.ImageListSerializer
    queryset = models.Image.objects.all()

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        images = models.Image.objects.filter(type=serializer.validated_data.get('image_type'))
        data = [image.to_json() for image in images]
        return Response(data, status=status.HTTP_200_OK)


class ImageDelete(generics.GenericAPIView):
    serializer_class = serializers.ImageDeleteSerializer
    queryset = models.Image.objects.all()

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        image = models.Image.objects.get(id=ObjectId(serializer.data.get('id')))
        return Response(image.to_json(), status=status.HTTP_200_OK)


class ImageEdit(generics.GenericAPIView):
    serializer_class = serializers.ImageEditSerializer
    queryset = models.Image.objects.all()

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        image = models.Image.objects.get(id=ObjectId(serializer.data.get('id')))
        return Response(image.to_json(), status=status.HTTP_200_OK)


class ImageDetail(generics.GenericAPIView):
    serializer_class = serializers.ImageDetailSerializer
    queryset = models.Image.objects.all()

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        image = models.Image.objects.get(id=ObjectId(serializer.data.get('id')))
        return Response(image.to_json(), status=status.HTTP_200_OK)



