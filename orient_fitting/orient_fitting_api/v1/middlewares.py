from django.utils.deprecation import MiddlewareMixin
from django.utils.functional import SimpleLazyObject

from v1.admin.models import Admin


def get_admin(request):

    # check admin in session
    if 'admin' in list(request.session.keys()) and request.session.get('admin'):
        admin = request.session.get('admin')
    else:
        return False

    # if admin exist in session make sure exist in db as well
    try:
        admin = Admin.objects.get(username=admin['username'])
    except Admin.DoesNotExist:
        admin = False
    return admin


class AdminAuthenticationMiddleware(MiddlewareMixin):

    @staticmethod
    def process_request(request):
        assert hasattr(request, 'session'), \
            "The Django authentication middleware requires session middleware to be " \
            "installed. Edit your MIDDLEWARE_CLASSES setting to insert " \
            "'django.contrib.sessions.middleware.SessionMiddleware'."

        request.admin = SimpleLazyObject(lambda: get_admin(request))
