"""
These are API URLs which will be used throughout our current application
"""

USER_LIST = 'v1/user/list'

USER_VIDEO_LIST = 'v1/video/list'

IMAGE_LIST = 'v1/image/list'
IMAGE_CREATE = 'v1/image/create'
IMAGE_EDIT = 'v1/image/edit'
IMAGE_DELETE = 'v1/image/delete'
IMAGE_DETAIL = 'v1/image/detail'

ADMIN_LOGIN = 'v1/admin/login'
