from django.urls import path
from v1.user import views

app_name = 'user'

urlpatterns = [
    path('list', views.UserList.as_view(), name='list'),
    path('<str:user_id>/videos', views.UserVideoList.as_view(), name='video-list'),
    path('login', views.AdminLogin.as_view(), name='admin-login'),
    path('logout', views.AdminLogout.as_view(), name='admin-logout')
]
