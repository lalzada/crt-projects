from django.views import generic
from django.conf import settings
from django.contrib import messages
from django.shortcuts import redirect, render
from django.urls import reverse
from django.contrib.auth.views import LoginView
from django.contrib.auth import REDIRECT_FIELD_NAME
from base.mixins import LoginRequiredMixin
import requests
import math
from v1 import endpoints
from .forms import AdminLoginForm


class AdminLogin(LoginView):
    template_name = 'user/admin_login.html'
    authentication_form = AdminLoginForm
    api_url = '{}{}'.format(settings.API_HOST_URL, endpoints.ADMIN_LOGIN)
    redirect_authenticated_user = True

    def is_logged_in(self):
        return 'user' in list(self.request.session.keys()) and \
               self.request.session.get('user')

    def dispatch(self, request, *args, **kwargs):
        if self.redirect_authenticated_user and self.is_logged_in():
            redirect_to = self.get_success_url()
            return redirect(redirect_to)
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        response = requests.post(self.api_url, form.cleaned_data)
        if response.status_code == 403:
            messages.warning(self.request, response.json()['detail'])
            context = self.get_context_data(form=form)
            return render(self.request, self.template_name, context)

        self.request.session['user'] = response.json()
        redirect_url = self.request.GET.get(REDIRECT_FIELD_NAME, settings.LOGIN_REDIRECT_URL)
        return redirect(redirect_url)


class AdminLogout(generic.View):

    @staticmethod
    def get(request):
        del request.session['user']
        messages.info(request, 'You are logged out')
        return redirect(settings.LOGIN_URL)


class PaginationContextMixin(object):
    total_pages = 0
    current_page = 1
    page_size = 10

    def get_context_data(self, *args, **kwargs):
        context = super(PaginationContextMixin, self).get_context_data(*args, **kwargs)
        context['next_page'] = int(self.current_page) + 1
        context['prev_page'] = int(self.current_page) - 1
        context['show_next_page'] = context.get('next_page') <= self.total_pages
        context['show_prev_page'] = context.get('prev_page') > 0
        return context


class UserList(LoginRequiredMixin, PaginationContextMixin, generic.ListView):

    template_name = 'user/list.html'
    api_url = '{}{}'.format(settings.API_HOST_URL, endpoints.USER_LIST)
    context_object_name = 'users'

    def get_queryset(self):
        page = self.request.GET.get('page', 1)
        page = int(page)
        page = page if page > 0 else 1
        response = requests.get('{}?page={}'.format(self.api_url, page))

        if response.status_code == 404:
            messages.info(self.request, 'No more users found')
            return redirect('{}?page={}'.format(reverse('v1:user:list'), page-1), permanent=True)

        response = response.json()
        self.total_pages = math.ceil(response['count']/self.page_size)
        self.current_page = page
        return response['results']


class UserVideoList(LoginRequiredMixin, PaginationContextMixin, generic.ListView):

    template_name = 'user/video_list.html'
    api_url = '{}{}'.format(settings.API_HOST_URL, endpoints.USER_VIDEO_LIST)
    context_object_name = 'videos'
    pk_url_kwarg = 'user_id'

    def get_queryset(self):
        page = self.request.GET.get('page', 1)
        page = int(page)
        page = page if page > 0 else 1
        data = {self.pk_url_kwarg: self.kwargs.get(self.pk_url_kwarg)}
        response = requests.post('{}?page={}'.format(self.api_url, page), data=data)

        if response.status_code == 404:
            messages.info(self.request, 'No more videos found')
            user_videos_url = reverse('v1:user:video-list', kwargs={'user_id': self.kwargs.get('user_id')})
            return redirect('{}?page={}'.format(user_videos_url, page-1), permanent=True)

        response = response.json()
        self.total_pages = math.ceil(response['count'] / self.page_size)
        self.current_page = page
        return response['results']

