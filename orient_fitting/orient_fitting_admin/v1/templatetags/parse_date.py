from django import template
from django.utils.dateparse import parse_datetime

register = template.Library()


@register.filter
def parse_date(date_str):
    return parse_datetime(date_str)
