from django.urls import path, include

app_name = 'v1'

urlpatterns = [
    path('user/', include('v1.user.urls', namespace='user')),
    path('image/', include('v1.image.urls', namespace='image'))
]
