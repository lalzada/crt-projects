from django import forms


class ImageAddForm(forms.Form):
    IMAGE_CHOICES = (
        ('splash', 'Splash'),
        ('logo', 'Logo'),
        ('scroll_category', 'Scroll Category')
    )

    type = forms.ChoiceField(choices=IMAGE_CHOICES, widget=forms.Select(attrs={'class': 'form-control'}))
    data = forms.CharField(initial='', widget=forms.Textarea(attrs={'class': 'form-control'}))
    mime_type = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    display_order = forms.IntegerField(min_value=0, widget=forms.NumberInput(attrs={'class': 'form-control'}))
    is_deleted = forms.BooleanField(required=False, widget=forms.CheckboxInput(attrs={'class': 'form-control'}))
    id = forms.CharField(widget=forms.HiddenInput(), required=False)

