from django.urls import path
from v1.image import views

app_name = 'image'

urlpatterns = [
    path('list/<str:image_type>', views.ImageList.as_view(), name='list'),
    path('delete/<str:image_id>', views.ImageDelete.as_view(), name='delete'),
    path('add', views.ImageAdd.as_view(), name='add'),
    path('edit/<str:image_id>', views.ImageEdit.as_view(), name='edit')
]
