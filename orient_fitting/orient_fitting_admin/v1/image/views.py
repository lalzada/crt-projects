from django.views import generic
from django import views
from django.conf import settings
from django.contrib import messages
from django.shortcuts import redirect, render
from django.urls import reverse
import requests
from base.mixins import LoginRequiredMixin

from .forms import ImageAddForm
from v1 import endpoints


def get_headers(request):
    return {'Authorization': 'Token {}'
            .format(request.session['user']['token'])}


class ImageList(LoginRequiredMixin, generic.ListView):

    template_name = 'image/list.html'
    api_url = '{}{}'.format(settings.API_HOST_URL, endpoints.IMAGE_LIST)
    context_object_name = 'img'
    pk_url_kwarg = 'image_type'

    def get_queryset(self):
        image_type = self.kwargs.get(self.pk_url_kwarg)
        data = {self.pk_url_kwarg: image_type if image_type else 'splash'}
        response = requests.post(self.api_url, data)
        return response.json()

    def get_context_data(self, *args, **kwargs):
        context = super(ImageList, self).get_context_data(*args, **kwargs)
        context['image_type'] = self.kwargs.get(self.pk_url_kwarg)
        return context


class ImageDelete(LoginRequiredMixin, views.View):

    api_url = '{}{}'.format(settings.API_HOST_URL, endpoints.IMAGE_DELETE)

    def post(self, request, image_id):
        requests.post(self.api_url, {'id': image_id}, headers=get_headers(request))
        return redirect(request.META.get('HTTP_REFERER'))


class ImageAdd(LoginRequiredMixin, generic.FormView):

    api_url = '{}{}'.format(settings.API_HOST_URL, endpoints.IMAGE_CREATE)
    template_name = 'image/add.html'
    form_class = ImageAddForm

    def form_valid(self, form):
        response = requests.post(self.api_url, form.cleaned_data,
                                 headers=get_headers(self.request))

        if response.status_code == 400:
            for field_name in response.json().keys():
                form.add_error(field_name, response.json()[field_name])
            context = self.get_context_data(form=form)
            return render(self.request, self.template_name, context)

        if response.status_code == 401:
            messages.warning(self.request, response.json()['detail'])
            context = self.get_context_data(form=form)
            return render(self.request, self.template_name, context)

        messages.success(self.request, 'Image added successfully.')
        return redirect(reverse('v1:image:list', kwargs={'image_type': form.cleaned_data.get('type')}))


class ImageEdit(LoginRequiredMixin, generic.FormView):

    api_url = '{}{}'.format(settings.API_HOST_URL, endpoints.IMAGE_EDIT)
    image_detail_url = '{}{}'.format(settings.API_HOST_URL, endpoints.IMAGE_DETAIL)
    template_name = 'image/edit.html'
    form_class = ImageAddForm

    def get_form_kwargs(self):
        kwargs = super(ImageEdit, self).get_form_kwargs()
        response = requests.post(self.image_detail_url,
                                 {'id': self.kwargs.get('image_id')},
                                 headers=get_headers(self.request))

        if response.status_code == 404:
            return redirect(self.request.META.get('HTTP_REFERER'))

        kwargs['initial'] = response.json()
        return kwargs

    def form_valid(self, form):
        response = requests.post(self.api_url, form.cleaned_data)
        if response.status_code == 400:
            for field_name in response.json().keys():
                form.add_error(field_name, response.json()[field_name])

            context = self.get_context_data(form=form)
            return render(self.request, self.template_name, context)

        messages.success(self.request, 'Image edited successfully.')
        return redirect(reverse('v1:image:list', kwargs={'image_type': form.cleaned_data.get('type')}))
