from .base import *

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'm7w)#t09wwzje4ipa2)2npl3#b&mn-6gb^d@7lbx-&kk4&3l_('

API_HOST_URL = 'http://localhost:8083/orient-fitting-api/'

ALLOWED_HOSTS = ['*']

DEBUG = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.0/howto/static-files/

STATIC_ROOT = os.path.join(BASE_DIR, '..', 'static')

STATIC_URL = '/static/'

if not DEBUG:
    INSTALLED_APPS += ['django.contrib.staticfiles']

