from .base import *
import environ


# https://github.com/joke2k/django-environ
# This file contains all the required options which needs to be set from
# Environment variables. This is a best practice to read secret and confidentials
# informations from a separate file which needs to be access restricted.
# This is one of the best practice mentioned by 12 Factor
# https://12factor.net/#the_twelve_factors

# Do not to store your .env files in version control where other developers would have
# access to your sensitive api keys and encryption keys.

# this file should not go in version control

root = environ.Path(__file__) - 2  # 2 folder back (/a/b/ - 2 = /)
env = environ.Env(DEBUG=(bool, False),)  # set default values and casting
environ.Env.read_env()  # reading .env file

SITE_ROOT = root()

# Raises ImproperlyConfigured exception if SECRET_KEY not in os.environ
SECRET_KEY = env('SECRET_KEY')

DEBUG = False

ALLOWED_HOSTS = ['127.0.0.1', 'localhost']

SECURE_CONTENT_TYPE_NOSNIFF = True

SECURE_BROWSER_XSS_FILTER = True

SESSION_COOKIE_SECURE = True

X_FRAME_OPTIONS = 'DENY'

# ADMINS will be notified of 500 errors by email.
ADMINS = [
    ('Imran Ilyas', 'imran.ilyas@cressettech.co'),
    ('Lal Zada', 'lal.zada@cressettech.co')
]


# MANAGERS will be notified of 404 errors
MANAGERS = [
    ('Imran Ilyas', 'imran.ilyas@cressettech.co'),
    ('Lal Zada', 'lal.zada@cressettech.co')
]

if not DEBUG:
    INSTALLED_APPS += ['django.contrib.staticfiles']
