from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from v1.user.views import UserList

urlpatterns = [
    path('', UserList.as_view()),
    path('v1/', include('v1.urls', namespace='v1')),
] \
              + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


