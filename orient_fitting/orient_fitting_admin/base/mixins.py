from django.contrib.auth.views import redirect_to_login
from django.contrib import messages


class LoginRequiredMixin(object):
    def dispatch(self, request, *args, **kwargs):
        if 'user' not in list(request.session.keys()) or \
                not request.session.get('user'):

            messages.info(request, 'Please login to proceed')
            return redirect_to_login(self.request.get_full_path())

        return super().dispatch(request, *args, **kwargs)
