from google2pandas import *
import json
import os
import multiprocessing as mp
from apiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials

SCOPES = ['https://www.googleapis.com/auth/analytics.readonly']
KEY_FILE_LOCATION = 'credentials.json'
VIEW_ID = '169170183'  # '121241460'  # '121241460'


def initialize_analyticsreporting():
    credentials = ServiceAccountCredentials.from_json_keyfile_name(
      KEY_FILE_LOCATION, SCOPES)

    # Build the service object.
    analytics = build('analyticsreporting', 'v4', credentials=credentials)

    return analytics


root_dir = os.path.dirname(os.path.abspath(__file__))

# dimensions_final = os.path.join(root_dir, 'dimensions_final.json')

dimensions_file = os.path.join(root_dir, 'dimensions_final.json')

with open(dimensions_file, 'r') as f:
    dimensions = json.load(f)

# analytics = initialize_analyticsreporting()


lock = mp.Lock()


def fetch_metric_list():

    for dimension, metrics in dimensionsitems():

        # for dimension, metrics in dimensions.items():
        for metric in metrics:

            query = {
                'reportRequests': [
                    {
                        'viewId': VIEW_ID,
                        'dateRanges': [{'startDate': '2018-02-02', 'endDate': '2018-02-02'}],
                        'dimensions': [{'name': dimension}],
                        'metrics': [metric]
                    },

                    {
                        'viewId': VIEW_ID,
                        'dateRanges': [{'startDate': '2018-02-03', 'endDate': '2018-02-03'}],
                        'dimensions': [{'name': dimension}],
                        'metrics': [metric]
                    }
                ]
            }

            conn = GoogleAnalyticsQueryV4(secrets='api_credentials.json')

            try:

                df = conn.execute_query(query)
                print(df)

            except Exception as e:
                print()
                print('--------------------------------------')
                print('ERROR')
                print(e)
                print(dimension)
                print(metric)
                print('--------------------------------------')
                print()


# pool = mp.Pool(processes=2)
#
# lst = [{d: m} for d, m in dimensions.items()]

# pool.map(fetch_metric_list, lst)

fetch_metric_list()
