from google2pandas import *
import json
import os
import multiprocessing as mp
from apiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials

SCOPES = ['https://www.googleapis.com/auth/analytics.readonly']
KEY_FILE_LOCATION = 'clickcue.json'
VIEW_ID = '169185147' #'169170183'  # '121241460'  # '121241460'


def initialize_analyticsreporting():
    credentials = ServiceAccountCredentials.from_json_keyfile_name(
      KEY_FILE_LOCATION, SCOPES)

    # Build the service object.
    analytics = build('analyticsreporting', 'v4', credentials=credentials)

    return analytics


root_dir = os.path.dirname(os.path.abspath(__file__))

dimensions_final = os.path.join(root_dir, 'dimensions_final.json')

dimensions_file = os.path.join(root_dir, 'dimensions.json')

with open(dimensions_file, 'r') as f:
    dimensions = json.load(f)

analytics = initialize_analyticsreporting()


lock = mp.Lock()


def fetch_metric_list(item):

    for dimension, metrics in item.items():
        pass

    # for dimension, metrics in dimensions.items():
    for metric in metrics:

            # query = {
            #     'reportRequests': [
            #         {
            #             'viewId': '169107699',
            #             'dateRanges': [{'startDate': '2018-02-02', 'endDate': '2018-02-02'}],
            #             'dimensions': [{'name': dimension}],
            #             'metrics': [metric]
            #         }
            #     ]
            # }
            #
            # conn = GoogleAnalyticsQueryV4(secrets='credentials_.json')

            try:

                # conn.execute_query(query)

                print()
                print('--------------------------------------')

                lock.acquire()

                with open(dimensions_final, 'r') as fuss:
                    saved_dimensions = json.load(fuss)

                lock.release()

                if dimension in saved_dimensions.keys():

                    if metric not in saved_dimensions[dimension]:
                        print('metric not found in saved dims. calling api', metric)
                        res = analytics.reports().batchGet(
                            body={
                                'reportRequests': [
                                    {
                                        'viewId': VIEW_ID,
                                        'dateRanges': [{'startDate': '2018-02-02', 'endDate': 'today'}],
                                        'metrics': [metric],
                                        'dimensions': [{'name': dimension}]
                                    }]
                            }
                        ).execute()

                        print('SUCCESS')
                        print(dimension)
                        print(res)

                        print('new Metric saved.', metric)
                        saved_dimensions[dimension].append(metric)
                    else:
                        print('Metric already exist in saved dims', metric)

                else:
                    saved_dimensions[dimension] = []
                    saved_dimensions[dimension].append(metric)
                    print('dim not found in saved dims', dimension)
                    print('calling api')
                    res = analytics.reports().batchGet(
                        body={
                            'reportRequests': [
                                {
                                    'viewId': VIEW_ID,
                                    'dateRanges': [{'startDate': '2018-02-02', 'endDate': 'today'}],
                                    'metrics': [metric],
                                    'dimensions': [{'name': dimension}]
                                }]
                        }
                    ).execute()

                    print('SUCCESS')
                    print(dimension)
                    print(res)

                    print('New dimension added', dimension)

                lock.acquire()
                with open(dimensions_final, 'w') as puss:
                    puss.write(json.dumps(saved_dimensions))

                lock.release()

                print('--------------------------------------')
                print()

            except Exception as e:
                print()
                print('--------------------------------------')
                print('ERROR')
                print(e)
                print(dimension)
                print(metric)
                print('--------------------------------------')
                print()

            # remove current metric from file

            lock.acquire()
            with open(dimensions_file, 'r') as r_file:
                dims = json.load(r_file)

            with open(dimensions_file, 'w') as w_file:
                dims[dimension].remove(metric)
                w_file.write(json.dumps(dims))
                print('metric removed', metric)

            lock.release()


pool = mp.Pool(processes=2)

lst = [{d: m} for d, m in dimensions.items()]

pool.map(fetch_metric_list, lst)

# fetch_metric_list()
