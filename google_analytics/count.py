import json
import os

root_dir = os.path.dirname(os.path.abspath(__file__))

dimensions_final = os.path.join(root_dir, 'dimensions_final.json')

with open(dimensions_final, 'r') as f:
    dimensions = json.load(f)


for dimension, metrics in dimensions.items():
    print(dimension)
    print(len(metrics))
    print('-----------------------')

print(max([len(metrics) for d, metrics in dimensions.items()]))
