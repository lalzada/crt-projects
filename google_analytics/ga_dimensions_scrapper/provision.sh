#!/bin/bash
sudo apt-get update
sudo apt-get -y install python3 python3-pip python3-dev build-essential libssl-dev libffi-dev xvfb

wget "https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb"
sudo dpkg -i google-chrome-stable_current_amd64.deb
sudo rm google-chrome-stable_current_amd64.deb
sudo apt-get install -y -f

# install and setup virtual env
pip3 install virtualenv

virtualenv /home/vagrant/venv/ga_dimensions_scrapper -p python3

source /home/vagrant/venv/ga_dimensions_scrapper/bin/activate

cd /vagrant/ga_dimensions_scrapper

pip3 install -r requirements.txt

echo "source /home/vagrant/venv/ga_dimensions_scrapper/bin/activate" >> /home/vagrant/.profile
echo "cd /vagrant/ga_dimensions_scrapper" >> /home/vagrant/.profile
echo "python3 scrapper.py" >> /home/vagrant/.profile

