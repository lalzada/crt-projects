import os
from time import sleep
from random import randint
from selenium import webdriver
from pyvirtualdisplay import Display
from sys import platform


class GoogleAnalyticsSpider(object):
    def __init__(self):
        self.url_to_crawl = "https://developers.google.com/analytics/devguides/reporting/core/dimsmets"
        self.driver = None
        self.all_items = []
        self.metric_count = 0
        self.current_dimension = None
        self.chrome_driver = None

    # Open headless chromedriver
    def start_driver(self):
        print('starting spider...')
        self.display = Display(visible=0, size=(800, 600))
        self.display.start()
        self.driver = webdriver.Chrome(self.chrome_driver)
        sleep(4)

    # Close chromedriver
    def close_driver(self):
        print('closing spider...')
        self.display.stop()
        self.driver.quit()
        print('closed!')

    # Tell the browser to get a page
    def get_page(self, url):
        print('getting page... {}'.format(self.url_to_crawl))
        self.driver.get(url)
        sleep(randint(2, 3))

    def get_dimension_list(self):

        # click on Expand all checkbox
        expand_all_btn = self.driver.find_element_by_css_selector('input#showHideAll')
        self.driver.execute_script("arguments[0].click();", expand_all_btn)

        # loop through each dimension
        for dimension_set in self.driver.find_elements_by_css_selector('div.dim'):

            for dimension in dimension_set.find_elements_by_css_selector('div.chkLabel'):

                dimension_checkbox = dimension.find_element_by_css_selector('input.chkCombo')
                self.driver.execute_script("arguments[0].click();", dimension_checkbox)

                dimension_name = dimension.find_element_by_css_selector('a.chkName').get_attribute("text")

                self.metric_count = 0

                self.current_dimension = dimension_name
                self.get_metric_list()

                print('--------------------------------------')

    def get_metric_list(self):

        print('grabbing list of metrics for {}'.format(self.current_dimension))

        # loop through each metric for this dimension
        for metric_set in self.driver.find_elements_by_css_selector('div.met'):

            for metric in metric_set.find_elements_by_css_selector('div.chkLabel'):
                sleep(0.1)

                if 'hideSlice' not in metric.get_attribute('class'):
                    metric_name = metric.find_element_by_css_selector('a.chkName').get_attribute('text')
                    self.metric_count += 1
                    print('====== {})'.format(self.metric_count), metric_name)
                else:
                    continue

    def parse(self):
        self.start_driver()
        self.get_page(self.url_to_crawl)
        self.get_dimension_list()
        self.close_driver()


if __name__ == '__main__':
    spider = GoogleAnalyticsSpider()

    project_dir = os.path.dirname(os.path.abspath(__file__))
    chrome_driver = None

    if platform == "linux" or platform == "linux2":
        chrome_driver = os.path.join(project_dir, 'chromedriver')

    elif platform == "win64" or platform == "win32":
        chrome_driver = os.path.join(project_dir, 'chromedriver_windows.exe')
        # raise OSError("We don't give a fu** to windows. Switch to linux please.")

    if not os.path.isfile(chrome_driver):
        raise FileExistsError('Please get a chrome driver and put it in project root dir with name '
                              '"chromedriver"')

    spider.chrome_driver = chrome_driver

    spider.parse()
